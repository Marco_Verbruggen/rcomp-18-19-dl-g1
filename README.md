﻿RCOMP 2018-2019 Project Repository
===========================================
# 1. Team members #
  * 1170623 - Marco Verbruggen
  * 1170677 - Tiago Andrade
  * 1170614 - João Silva
  * 1161274 - Artur Muiria
  * 1170521 - Pedro Carvalho
  * 1170944 - Alexandre Herculano

# 2. Sprints #
  * [Sprint 1](doc/sprint1/)
  * [Sprint 2](doc/sprint2/)
  * [Sprint 3](doc/sprint3/)
  * [Sprint 4](doc/sprint4/)
  * [Sprint 5](doc/sprint5/)
  * [Sprint 6](doc/sprint6/)
