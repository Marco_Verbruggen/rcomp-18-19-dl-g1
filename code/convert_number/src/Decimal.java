public class Decimal extends NumberConverter{

    public Decimal(int numericValue){
        super(numericValue);
    }

    @Override
    public String toString() {
        return convert("DEC");
    }
}
