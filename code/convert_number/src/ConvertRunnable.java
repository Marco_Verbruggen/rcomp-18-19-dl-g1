import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;

public class ConvertRunnable implements Runnable {


    private DatagramSocket datagramSocket;


    public ConvertRunnable(DatagramSocket udp_s) {
        datagramSocket = udp_s;
    }

    @Override
    public void run() {
        byte[] data = new byte[300];
        String result;
        DatagramPacket udpPacket;
        InetAddress currPeerAddress;

        udpPacket = new DatagramPacket(data, data.length);

        while (true) {
            udpPacket.setLength(data.length);
            try {
                datagramSocket.receive(udpPacket);
            } catch (IOException ex) {
                return;
            }
            currPeerAddress = udpPacket.getAddress();

            if (data[0] == 1) { // peer start
                ConvertApplication.addIP(udpPacket.getAddress());
                try {
                    datagramSocket.send(udpPacket);
                } catch (IOException ex) {
                    return;
                }
            } else if (data[0] == 0) { // peer exit
                ConvertApplication.remIP(udpPacket.getAddress());
            } else {        // chat message
                result = new String(udpPacket.getData(), 0, udpPacket.getLength());
                System.out.println(result);
            }
        }
    }
}
