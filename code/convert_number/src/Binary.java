public class Binary extends NumberConverter{

    public Binary(int numericValue){
        super(numericValue);
    }

    @Override
    public String toString() {
        return convert("BIN");
    }
}
