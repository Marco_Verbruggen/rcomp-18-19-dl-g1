public class Hexadecimal extends NumberConverter {


    public Hexadecimal(int numericValue) {
        super(numericValue);
    }


    @Override
    public String toString() {
        return Integer.toHexString(numericValue);
    }
}