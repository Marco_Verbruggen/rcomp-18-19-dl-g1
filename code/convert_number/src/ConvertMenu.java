import java.util.Scanner;

public class ConvertMenu {

    Scanner in;

    public ConvertMenu(Scanner in) {
        this.in = in;
    }


    public String convert() {
        try {
            String base;
            String value;
            System.out.println("Introduce the base");
            base = in.nextLine();
            NumberConverter number = null;
            if (((base.equals("DEC")) || (base.equals("BIN")) || (base.equals("OCT")) || (base.equals("HEX")))) {
                System.out.println("Introduce the value");
                value = in.nextLine();
                switch (base) {
                    case "BIN":
                        number = new Binary(Integer.parseInt(value, 2));
                        break;
                    case "OCT":
                        number = new Octal(Integer.parseInt(value, 8));
                        break;
                    case "DEC":
                        number = new Decimal(Integer.parseInt(value));
                        break;
                    case "HEX":
                        number = new Hexadecimal(Integer.parseInt(value, 16));
                        break;
                    default:
                        break;
                }
                if (number != null) {
                    return "Binary Representation : " + number.convert("BIN") + "\n" + "Octal Representation : " + number.convert("OCT") + "\n" + "Decimal Representation : " + number.convert("DEC") + "\n" + "Hexadecimal Representation : " + number.convert("HEX");
                }
            } else {
                convert();
            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
            convert();
        }
        return null;
    }
}
