public class Octal extends NumberConverter{

    public Octal(int numericValue){
        super(numericValue);
    }

    @Override
    public String toString() {
        return convert("OCT");
    }
}
