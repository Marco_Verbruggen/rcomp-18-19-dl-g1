public class NumberConverter {
    int numericValue;

    public NumberConverter(int numericValue){
        this.numericValue = numericValue;
    }
    public String convert(String base) {
        switch (base) {
            case "DEC":
                return String.valueOf(numericValue);
            case "BIN":
                return Integer.toBinaryString(numericValue);
            case "OCT":
                return Integer.toOctalString(numericValue);
            case "HEX":
                return Integer.toHexString(numericValue);
        }
        return null;
    }
}
