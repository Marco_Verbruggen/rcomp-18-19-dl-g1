import java.util.Scanner;

public class Menu {

    public Menu(){

    }


    public String show(){
        Scanner in = new Scanner(System.in);
        String option = "";
        while(!option.equals("0")) {
            System.out.println(showOptions());
            option = in.nextLine();
            switch(option){
                case("1"):
                    return "LIST";
                case("2"):
                    return showList();
                case("3") :
                    return new ConvertMenu(in).convert();
                case ("0") :
                    return "EXIT";
            }
        }
        return null;
    }


    private String showOptions(){
        return "1 - List All Peers\n2 - List Valid Formats\n3 - Convert\n0 - End Program\n\nIntroduce the option : ";
    }

    private String showList(){
        return "BIN -> Binary\nOCT -> Octal\nDEC -> Decimal\nHEX -> Hexadecimal\n\n";
    }
}
