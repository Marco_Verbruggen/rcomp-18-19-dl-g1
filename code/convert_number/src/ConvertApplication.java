import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.util.HashSet;

public final class ConvertApplication {
    private static final String BROADCAST_ADDRESS = "255.255.255.255";

    private static final int SERVICE_PORT = 31201;

    static InetAddress broadcastAddress;

    static DatagramSocket sock;

    private static HashSet<InetAddress> peersList = new HashSet();

    public static synchronized void addIP(InetAddress ip) {
        peersList.add(ip);
    }

    public static synchronized void remIP(InetAddress ip) {
        peersList.remove(ip);
    }

    public static synchronized void printIPs() {
        for (InetAddress ip : peersList) {
            System.out.print(" " + ip.getHostAddress());
        }
    }

    public static synchronized void sendToAll(DatagramSocket s, DatagramPacket p) throws Exception {
        for (InetAddress ip : peersList) {
            p.setAddress(ip);
            s.send(p);
        }
    }


    public static void run() throws Exception {
        DatagramPacket udpPacket;
        byte[] data = new byte[300];

        try {
            sock = new DatagramSocket(SERVICE_PORT);
        } catch (IOException ex) {
            System.out.println("Failed to open local port");
            System.exit(1);
        }

        broadcastAddress = InetAddress.getByName(BROADCAST_ADDRESS);
        sock.setBroadcast(true);
        data[0] = 1;
        udpPacket = new DatagramPacket(data, 1, broadcastAddress, SERVICE_PORT);
        sock.send(udpPacket);

        Thread udpReceiver = new Thread(new ConvertRunnable(sock));
        udpReceiver.start();

        String phrase;
        byte[] phraseDate;
        while (true) { // handle user inputs
            phrase = new Menu().show();
            if (phrase.compareTo("EXIT") == 0) break;
            if (phrase.compareTo("LIST") == 0) {
                System.out.print("\nActive peers:");
                printIPs();
                System.out.println("\n");
            } else {
                phraseDate = phrase.getBytes();
                udpPacket.setData(phraseDate);
                udpPacket.setLength(phrase.length());
                sendToAll(sock, udpPacket);
            }
        }
        data[0] = 0; // announce I'm leaving
        udpPacket.setData(data);
        udpPacket.setLength(1);
        sendToAll(sock, udpPacket);
        sock.close();
        udpReceiver.join(); // wait for the thread to end
    }
}
