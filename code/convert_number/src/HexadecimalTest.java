import static org.junit.jupiter.api.Assertions.*;

class HexadecimalTest {

    @org.junit.jupiter.api.Test
    void convert() {
        Hexadecimal hex = new Hexadecimal(Integer.parseInt("FF", 16));
        assertEquals("ff", hex.convert("HEX"));
        assertNull(hex.convert("adjalskjfdas"));
        assertEquals("255", hex.convert("DEC"));
        assertEquals("377", hex.convert("OCT"));
        assertEquals("11111111", hex.convert("BIN"));
    }
}