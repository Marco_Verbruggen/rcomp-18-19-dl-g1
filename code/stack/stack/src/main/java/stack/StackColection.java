/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package stack;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Stack;

/**
 *
 * @author Tiago - PC
 */
public class StackColection {

    private Map<String, Stack<String>> allStacks;

    private static final String UNKNOWN_IDENTIFIER_MESSAGE
            = "THERE ISN'T A KNOWN STACK WITH THE SPECIFIED IDENTIFIER.";

    public StackColection() {
        allStacks = new HashMap<>();
    }

    public String push(String identifier, String message) {
        Stack<String> chosenStack;

        if (allStacks.containsKey(identifier)) {
            chosenStack = allStacks.get(identifier);

        } else {
            chosenStack = new Stack<>();
            allStacks.put(identifier, chosenStack);
        }

        chosenStack.push(message);
        return String.format("%d", chosenStack.size());

    }

    public String pop(String identifier) {
        if (allStacks.containsKey(identifier)) {
            Stack<String> chosenStack = allStacks.get(identifier);

            String messageToReturn = chosenStack.pop();
            if (chosenStack.empty()) {
                allStacks.remove(identifier);
            }
            return messageToReturn;
        }
        return UNKNOWN_IDENTIFIER_MESSAGE;
    }

    public String list() {
        String list = "";

        for (Entry<String, Stack<String>> entry : allStacks.entrySet()) {
            list += String.format("Identifier %s; Size %d;\n",
                    entry.getKey(), entry.getValue().size());
        }

        return list;
    }

}
