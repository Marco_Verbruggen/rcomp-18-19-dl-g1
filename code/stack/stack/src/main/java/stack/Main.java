package stack;

public class Main {

    public static void main(String[] args) {
        try {
            StackApplication.run();
        } catch (Exception ex) {
            System.out.println("ERROR: An error occurred during the process");
        }
    }
}
