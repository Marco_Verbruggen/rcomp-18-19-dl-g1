package stack;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;

public class StackRunnable implements Runnable {

    private DatagramSocket datagramSocket;

    private static final int SERVICE_PORT = 31201;

    public StackRunnable(DatagramSocket udp_s) {
        datagramSocket = udp_s;
    }

    @Override
    public void run() {
        byte[] data = new byte[300];
        String result;
        DatagramPacket udpPacket;
        InetAddress currPeerAddress;

        udpPacket = new DatagramPacket(data, data.length);

        while (true) {
            udpPacket.setLength(data.length);
            try {
                datagramSocket.receive(udpPacket);

                int usedPort = udpPacket.getPort();

                if (usedPort != SERVICE_PORT) {
                    throw new IOException(
                            String.format(
                                    "This packtet is not from the correct port number.\nPort expected %d;\n Port recivied %d;\n\n",
                                    SERVICE_PORT, usedPort));
                }

            } catch (IOException ex) {
                return;
            }
            currPeerAddress = udpPacket.getAddress();

            if (data[0] == 1) {
                // peer start
                StackApplication.addIP(udpPacket.getAddress());
                try {
                    datagramSocket.send(udpPacket);
                } catch (IOException ex) {
                    return;
                }
            } else if (data[0] == 0) { // peer exit
                StackApplication.remIP(udpPacket.getAddress());
            } else {        // chat message
                result = new String(udpPacket.getData(), 0, udpPacket.getLength());
                System.out.println(result);
            }
        }

    }
}
