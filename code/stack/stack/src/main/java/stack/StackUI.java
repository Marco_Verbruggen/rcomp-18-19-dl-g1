package stack;

import java.util.Scanner;

public class StackUI {

    private static final StackColection STACK_COLLECTION = new StackColection();

    private static final String MENU = ""
            + "==================================\n"
            + "               MENU\n"
            + "==================================\n";

    public synchronized String runProgram() {

        Scanner scanner = new Scanner(System.in);
        System.out.println(MENU
                + "0 - EXIT\n"
                + "1 - PUSH\n"
                + "2 - POP\n"
                + "3 - LIST ALL STACKS\n"
                + "4 - LIST ADDRESSES\n"
                + "\nPlease Insert Option: ");
        if (!scanner.hasNextInt()) {
            return runProgram();
        }

        int option = scanner.nextInt();

        switch (option) {
            case 0:
                return "EXIT";

            case 1:
                final String OPTION1_IDNETIFIER_MESSAGE
                        = "\nPlease write the identifier from the stack where you want to push.";

                final String OPTION1_MESSAGE
                        = "\nPlease write the message that you want to push to the stack.";

                return STACK_COLLECTION.push(
                        requestInput(OPTION1_IDNETIFIER_MESSAGE),
                        requestInput(OPTION1_MESSAGE));

            case 2:
                final String OPTION2_MESSAGE
                        = "\nPlease write the identifier from the stack where you want to pop.";
                return STACK_COLLECTION.pop(requestInput(OPTION2_MESSAGE));

            case 3:
                return STACK_COLLECTION.list();

            case 4:
                return "LIST";

            default:
                return runProgram();
        }
    }

    private String requestInput(String message) {
        Scanner scanner = new Scanner(System.in);

        do {
            System.out.println(message);
        } while (!scanner.hasNextLine());

        return scanner.nextLine();
    }

}
