package main.java;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.util.HashSet;

/**
 * RCOMP project 2018/2019.
 *
 * @author Pedro Carvalho
 */
public final class HashApplication {

    /**
     * Broadcast ip address.
     */
    private static final String BROADCAST_ADDRESS = "255.255.255.255";

    /**
     * Service port.
     */
    private static final int SERVICE_PORT = 31201;

    /**
     * Maximum package size.
     */
    private static final int MAXIMUM_BYTES = 500;

    /**
     * Broadcast Ip address.
     */
    private static InetAddress broadcastAddress;

    /**
     * Socket.
     */
    private static DatagramSocket sock;

    /**
     * List of ip addresses communicating with the application server.
     */
    private static HashSet<InetAddress> peersList = new HashSet();

    /**
     * Hidden constructor for utility class.
     */
    protected HashApplication() {

    }

    /**
     * Method that adds a new ip address to the list when
     * a new user access an application.
     *
     * @param ip new ip address
     */
    public static synchronized void addIP(final InetAddress ip) {
        peersList.add(ip);
    }

    /**
     * Method that removes an ip address of the list when
     * a user leaves the application.
     *
     * @param ip ip to remove
     */
    public static synchronized void remIP(final InetAddress ip) {
        peersList.remove(ip);
    }

    /**
     * Method that prints all the ip addresses using the applications.
     */
    public static synchronized void printIPs() {
        for (InetAddress ip : peersList) {
            System.out.print("\n - " + ip.getHostAddress());
        }
    }

    /**
     * Method that sends my ip address to all the other users.
     *
     * @param s socket
     * @param p packet
     * @throws Exception exception
     */
    public static synchronized void sendToAll(
            final DatagramSocket s,
            final DatagramPacket p) throws Exception {

        for (InetAddress ip : peersList) {
            p.setAddress(ip);
            s.send(p);
        }
    }


    /**
     * Method that runs the application and sends the packet
     * with the result data to the client side.
     *
     * @throws Exception exception
     */
    public void run() throws Exception {
        DatagramPacket udpPacket;
        byte[] data = new byte[MAXIMUM_BYTES];

        try {
            sock = new DatagramSocket(SERVICE_PORT);
        } catch (IOException ex) {
            System.out.println("Failed to open local port");
            System.exit(1);
        }

        broadcastAddress = InetAddress.getByName(BROADCAST_ADDRESS);
        sock.setBroadcast(true);
        data[0] = 1;
        udpPacket = new DatagramPacket(data, 1, broadcastAddress, SERVICE_PORT);
        sock.send(udpPacket);

        Thread udpReceiver = new Thread(new HashRunnable(sock));
        udpReceiver.start();

        sendAnnouncementMessage(sock, udpPacket);
        String result;
        byte[] resultBytes;
        while (true) { // handle user inputs
            result = new HashUI().runProgram();
            if (result.compareTo("EXIT") == 0) {
                break;
            }
            if (result.compareTo("LIST") == 0) {
                System.out.print("\nActive peers:");
                printIPs();
                System.out.println("\n");
            } else {
                resultBytes = result.getBytes();
                udpPacket.setData(resultBytes);
                udpPacket.setLength(result.length());
                sendToAll(sock, udpPacket);
            }
        }
        data[0] = 0; // announce I'm leaving
        udpPacket.setData(data);
        udpPacket.setLength(1);
        sendToAll(sock, udpPacket);
        sock.close();
        udpReceiver.join(); // wait for the thread to end
    }

    /**
     * Method that sends announcement message to all users.
     *
     * @param socket socket
     * @param packet packet
     * @throws Exception exception
     */
    private void sendAnnouncementMessage(DatagramSocket socket, DatagramPacket packet) throws Exception {
        String message = "\nIP Address " + peersList.iterator().next().toString() + " in the application " + this.getClass().getSimpleName() + " has joined\n";
        packet.setAddress(broadcastAddress);
        packet.setData(message.getBytes());
        packet.setLength(message.getBytes().length);
        socket.send(packet);
    }
}

