package main.java;

import org.apache.commons.codec.digest.DigestUtils;

/**
 * RCOMP project 2018/2019.
 *
 * @author Pedro Carvalho
 */
public final class HashAlgorithms {

    /**
     * Hidden constructor for utility class.
     */
    private HashAlgorithms() {

    }

    /**
     * Method that generates a MD5 hash code of some input.
     *
     * @param input received input
     * @return hash code
     */
    public static String md5(final String input) {
        return DigestUtils.md5Hex(input);
    }

    /**
     * Method that generates a SHA-0 hash code of some input.
     *
     * @param input received input
     * @return hash code
     */
    public static String sha0(final String input) {
        return DigestUtils.sha1Hex(input);
    }

    /**
     * Method that generates a SHA-256 hash code of some input.
     *
     * @param input received input
     * @return hash code
     */
    public static String sha256(final String input) {
        return DigestUtils.sha256Hex(input);
    }
}
