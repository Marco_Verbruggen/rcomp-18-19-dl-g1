package main.java;

import java.util.Scanner;

/**
 * RCOMP project 2018/2019.
 *
 * @author Pedro Carvalho
 */
public class HashUI {

    /**
     * Menu string.
     */
    private static final String MENU = ""
            + "==================================\n"
            + "               MENU\n"
            + "==================================\n";

    /**
     * Method that runs the application UI.
     *
     * @return application result
     */
    public synchronized String runProgram() {
        Hash hash = new Hash();

        Scanner scanner = new Scanner(System.in);
        System.out.println(MENU
                + "1 - LIST ALGORITHMS\n"
                + "2 - CALCULATE HASH CODE\n"
                + "3 - LIST ADDRESSES\n"
                + "0 - EXIT\n\n"
                + "Insert Option: ");
        if (!scanner.hasNextInt()) {
            return runProgram();
        }

        int option = scanner.nextInt();

        switch (option) {
            case 1:
                return hash.listHashAlgorithms();
            case 2:
                int algorithm = requestAlgorithmNumber();
                String input = requestInput();
                return "\nResult "
                        + hash.useHashAlgorithm(algorithm, input)
                        + "\n";
            case 3:
                return "LIST";
            case 0:
                return "EXIT";
            default:
                return runProgram();
        }
    }

    /**
     * Method that requests the user the algorithm number to use.
     *
     * @return chosen algorithm number
     */
    private int requestAlgorithmNumber() {
        Scanner scanner = new Scanner(System.in);
        System.out.print("\nInsert Algorithm Number: ");
        if (!scanner.hasNextInt()) {
            return requestAlgorithmNumber();
        }
        return scanner.nextInt();
    }

    /**
     * Method that requests the user the input to encode.
     *
     * @return input to encode
     */
    private String requestInput() {
        Scanner scanner = new Scanner(System.in);
        System.out.print("\nInput To Generate Hash: ");
        if (!scanner.hasNextLine()) {
            return requestInput();
        }
        return scanner.nextLine();
    }
}
