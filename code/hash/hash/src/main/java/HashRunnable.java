package main.java;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;

/**
 * RCOMP project 2018/2019.
 *
 * @author Pedro Carvalho
 */
public class HashRunnable implements Runnable {

    /**
     * Maximum package size.
     */
    private static final int MAXIMUM_BYTES = 500;

    /**
     * Socket.
     */
    private DatagramSocket datagramSocket;

    /**
     * Constructor that receives a socket by parameter.
     *
     * @param udpDatagram socket
     */
    public HashRunnable(final DatagramSocket udpDatagram) {
        datagramSocket = udpDatagram;
    }

    /**
     * Method that runs the client side.
     */
    @Override
    public void run() {
        byte[] data = new byte[MAXIMUM_BYTES];
        String result;
        DatagramPacket udpPacket;
        InetAddress currPeerAddress;

        udpPacket = new DatagramPacket(data, data.length);

        while (true) {
            udpPacket.setLength(data.length);
            try {
                datagramSocket.receive(udpPacket);
            } catch (IOException ex) {
                return;
            }
            currPeerAddress = udpPacket.getAddress();

            if (data[0] == 1) { // peer start
                HashApplication.addIP(udpPacket.getAddress());
                try {
                    datagramSocket.send(udpPacket);
                } catch (IOException ex) {
                    return;
                }
            } else if (data[0] == 0) { // peer exit
                HashApplication.remIP(udpPacket.getAddress());
            } else {        // chat message
                result = new String(udpPacket.getData(),
                        0,
                        udpPacket.getLength());
                System.out.println(result);
            }
        }

    }
}
