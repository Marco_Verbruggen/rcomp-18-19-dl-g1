package main.java;

/**
 * RCOMP project 2018/2019.
 *
 * @author Pedro Carvalho
 */
public class Main {

    /**
     * Hidden constructor for utility class.
     */
    protected Main() {

    }

    /**
     * Main method.
     *
     * @param args arguments
     */
    public static void main(final String[] args) {
        try {
            new HashApplication().run();
        } catch (Exception ex) {
            System.out.println("ERROR: An error occurred during the process");
        }
    }
}
