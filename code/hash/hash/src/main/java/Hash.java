package main.java;

/**
 * RCOMP project 2018/2019.
 *
 * @author Pedro Carvalho
 */
public class Hash {

    /**
     * Error message when chosen algorithm does not exist.
     */
    private static final String ERROR_MESSAGE =
            "ERROR: Algorithm not Supported";

    /**
     * Enumeration of available algorithms.
     */
    private enum HashEnum {
        MD5, SHA0, SHA256
    }

    /**
     * Method that lists all the available algorithms.
     *
     * @return string with all the algorithms
     */
    public String listHashAlgorithms() {
        StringBuilder stringBuilder = new StringBuilder();

        stringBuilder.append("\nHASH ALGORITHMS\n\n");

        HashEnum[] values = HashEnum.values();
        for (int i = 0; i < values.length; i++) {
            stringBuilder.append(i + 1);
            stringBuilder.append(" - " + values[i] + "\n");
        }

        return stringBuilder.toString();
    }

    /**
     * Method that generates an hash code of a received input.
     *
     * @param algorithm type of algorithm to use
     * @param input     received input to encode
     * @return hash code
     */
    public String useHashAlgorithm(final int algorithm, final String input) {
        switch (algorithm) {
            case 1:
                return HashEnum.MD5 + " Encoding: "
                        + HashAlgorithms.md5(input);
            case 2:
                return HashEnum.SHA0 + " Encoding: "
                        + HashAlgorithms.sha0(input);
            case 3:
                return HashEnum.SHA256 + " Encoding: "
                        + HashAlgorithms.sha256(input);
            default:
                return ERROR_MESSAGE;
        }
    }
}
