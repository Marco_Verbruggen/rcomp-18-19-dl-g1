/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package isep.ipp.pt.rcomp5;

import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.TreeMap;

/**
 *
 * @author Joao Silva
 */
public class TCPMonitorList {

    private static final String SUCCESS_MESSAGE = "Done with success.";
    private static final String ERROR_MESSAGE = "Indentifier doesn't exist";

    private List<TCPMonitor> tcpMonitorList;

    public TCPMonitorList(List<TCPMonitor> tcpMonitorList) {
        this.tcpMonitorList = tcpMonitorList;
    }

    /**
     * @return the tcpMonitorList
     */
    public List<TCPMonitor> getTcpMonitorList() {
        return tcpMonitorList;
    }

    public String addService(String id, String ip, int tcpNumber) {
        TCPMonitor tcpM = new TCPMonitor(id, ip, tcpNumber);
        if (!tcpMonitorList.contains(tcpM)) {
            tcpMonitorList.add(tcpM);
        }
        return SUCCESS_MESSAGE;
    }

    public String removeService(String id) {
        for (TCPMonitor tcpMonitor : tcpMonitorList) {
            if (tcpMonitor.getIdentifier().equals(tcpMonitor.getIdentifier())) {
                tcpMonitorList.remove(tcpMonitor);

                return SUCCESS_MESSAGE;
            }
        }

        return ERROR_MESSAGE;
    }

    public Map<String, String> checkStatus(Map<String, String> mapTCP) {

        int rand = 0;
        for (String string : mapTCP.keySet()) {
            rand = randomInt();
            if (rand <= 5) {//connect   socket.isConnected()
                mapTCP.put(string, "available");
            } else {//cant connect
                mapTCP.put(string, "unavailable");
            }
        }
        return mapTCP;
    }

    public Map<String, String> fillStatus() {
        Map<String, String> mapTCP = new TreeMap<>();
        for (TCPMonitor tCPMonitor : tcpMonitorList) {
            mapTCP.put(tCPMonitor.getIdentifier(), "unchecked");
        }
        return mapTCP;
    }

    public int randomInt() {
        Random ran = new Random();
        return ran.nextInt(10);
    }

}
