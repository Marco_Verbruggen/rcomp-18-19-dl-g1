/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package isep.ipp.pt.rcomp5;

import java.util.Map;
import java.util.Scanner;

/**
 *
 * @author Joao Silva
 */
public class TCPMonitorUI {

    private static final String MENU = ""
                    + "==================================\n"
                    + "               MENU\n"
                    + "==================================\n";

    public synchronized String runProgram(TCPMonitorList tcpList) {
        Scanner scanner = new Scanner(System.in);
        System.out.println(MENU + "1 - Add Service\n2 - Remove Service\n3 - Service Status\n4 - List Addresses\n0 - EXIT\n\nInsert Option: ");
        if (!scanner.hasNextInt()) {
            return runProgram(tcpList);
        }

        int option = scanner.nextInt();

        switch (option) {
            case 1:
                String phrase = "Insert identifier:";
                String id = requestInput(phrase);
                phrase = "Insert ip adress:";
                String ip = requestInput(phrase);
                phrase = "Insert TCP number:";
                int tcpN = requestNumber(phrase);
                String res = tcpList.addService(id, ip, tcpN);

                return "\n" + res + "\n";

            case 2:
                phrase = "Insert identifier:";
                id = requestInput(phrase);
                res = tcpList.removeService(id);

                return "\n" + res + "\n";
            case 3:
                Map<String, String> mapTCP = tcpList.fillStatus();

                return "\n" + tcpList.checkStatus(mapTCP) + "\n";
            case 4:
                return "LIST";

            case 0:
                return "EXIT";
            default:
                return runProgram(tcpList);
        }

    }

    private String requestInput(String phrase) {
        Scanner scanner = new Scanner(System.in);
        System.out.printf("\n%s\n", phrase);
        if (!scanner.hasNextLine()) {
            return requestInput(phrase);
        }
        return scanner.nextLine();
    }

    private int requestNumber(String phrase) {
        Scanner scanner = new Scanner(System.in);
        System.out.printf("\n%s\n", phrase);
        if (!scanner.hasNextInt()) {
            return requestNumber(phrase);
        }
        return scanner.nextInt();
    }

}
