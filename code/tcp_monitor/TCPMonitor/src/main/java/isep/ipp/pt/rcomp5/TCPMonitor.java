/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package isep.ipp.pt.rcomp5;

/**
 *
 * @author Joao Silva
 */
public class TCPMonitor {

    private String identifier;
    private String ipAdress;
    private int tcpPortNumber;

    public TCPMonitor(String identifier, String ipAdress, int tcpPortNumber) {
        this.identifier = identifier;
        this.ipAdress = ipAdress;
        this.tcpPortNumber = tcpPortNumber;
    }

    public String getIdentifier() {
        return identifier;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof TCPMonitor)) {
            return false;
        }
        TCPMonitor otherTCP = (TCPMonitor) o;
        return identifier.equals(otherTCP.identifier)
                        && ipAdress.equals(otherTCP.ipAdress)
                        && tcpPortNumber == otherTCP.tcpPortNumber;
    }

}
