/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package isep.ipp.pt.rcomp5;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;

/**
 *
 * @author Joao Silva
 */
public class TCPMonitorRunnable implements Runnable {

    private DatagramSocket datagramSocket;

    private static final int SERVICE_PORT = 31201;

    public TCPMonitorRunnable(DatagramSocket udp_s) {
        datagramSocket = udp_s;
    }

    @Override
    public void run() {
        byte[] data = new byte[300];
        String result;
        DatagramPacket udpPacket;
        InetAddress currPeerAddress;

        udpPacket = new DatagramPacket(data, data.length);

        while (true) {
            udpPacket.setLength(data.length);
            try {
                datagramSocket.receive(udpPacket);
                if (udpPacket.getPort() != SERVICE_PORT) {
                    return;
                }
            } catch (IOException ex) {
                return;
            }
            currPeerAddress = udpPacket.getAddress();

            if (data[0] == 1) { // peer start
                TCPMonitorApplication.addIP(udpPacket.getAddress());
                try {
                    datagramSocket.send(udpPacket);
                } catch (IOException ex) {
                    return;
                }
            } else if (data[0] == 0) { // peer exit
                TCPMonitorApplication.remIP(udpPacket.getAddress());
            } else {        // chat message
                result = new String(udpPacket.getData(), 0, udpPacket.getLength());
                System.out.println(result);
            }
        }

    }
}
