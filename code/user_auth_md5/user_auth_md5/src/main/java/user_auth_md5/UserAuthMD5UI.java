/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package user_auth_md5;

import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.util.InputMismatchException;
import java.util.Scanner;

/**
 *
 * @author Artur Muiria
 */
public class UserAuthMD5UI {

    private static Scanner in = new Scanner(System.in);
    private static Scanner myObj = new Scanner(System.in);

     public synchronized String runProgram() throws NoSuchAlgorithmException, IOException {
        
        System.out.println(" -----##### UserAuthMD5 #####-----\n");
        UserAuthMD5 newMD5 = new UserAuthMD5();
        String token;
        String username;
        String password;
        int opt = 2;
        while (opt != 0) {
            boolean numb = false;
            while (!numb) {
                System.out.println("\nMenu: \n");
                System.out.println("1 - GetAuthToken");
                System.out.println("2 - Authenticate");
                System.out.println("Choose a number: (0 to go back)");
                try {
                    opt = in.nextInt();
                    numb = true;
                } catch (InputMismatchException e) {
                    numb = false;
                    System.out.println("Choose a correct number !");
                    in.nextLine();
                }
            }
            switch (opt) {
                case 0:
                    System.out.println("Main menu");
                    break;
                case 1:
                    System.out.println("Insert username:");
                    username = myObj.nextLine();
                    Boolean exist = newMD5.exist(username);
                    if(exist){
                        token = newMD5.generateAuthToken(username);
                        System.out.println("Here is your token: " + token);
                        break;
                    }else{
                        System.out.println("The username doesn't exist, but we'll give you a token anyway.");
                        token = newMD5.generateAuthTokenNouser(username);
                        System.out.println("Here is your token: " + token);
                        break;
                    }
                case 2:
                    System.out.println("Insert username:");
                    username = myObj.nextLine();
                    System.out.println("Insert Password:");
                    password = myObj.nextLine();
                    System.out.println("Insert AuthToken given previously:");
                    token = myObj.nextLine();
                    
                    newMD5.cheackUser(username,password,token);
                    
                    break;
                default:
                    System.out.println("\nOption does not exit!\n");
                    break;
            }
        }
        return runProgram();
    }
}
