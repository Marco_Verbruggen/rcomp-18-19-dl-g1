/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package user_auth_md5;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;

/**
 *
 * @author Artur Muiria
 */
public class UserAuthMD5Runnable implements Runnable {    
    private DatagramSocket s;

    public UserAuthMD5Runnable(DatagramSocket udp_s) { 
        s=udp_s;
    }

    public void run() {
        int i;
        byte[] data = new byte[300];
        String frase;
        DatagramPacket p;
        InetAddress currPeerAddress;

        p=new DatagramPacket(data, data.length);

        while(true) {
            p.setLength(data.length);
            try { 
                s.receive(p); 
            } catch(IOException ex) { 
                return; 
            }
            currPeerAddress=p.getAddress();

            if(data[0]==1) { // peer start
                UserAuthMD5Application.addIP(p.getAddress());
                try { 
                    s.send(p); 
                } catch(IOException ex) { 
                    return; 
                }
            } else if(data[0]==0) { // peer exit
                UserAuthMD5Application.remIP(p.getAddress());
            } else {		// chat message
                frase = new String( p.getData(), 0, p.getLength());
                System.out.println(frase);
            }
        }
    }    
}
