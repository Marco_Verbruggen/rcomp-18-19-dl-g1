package user_auth_md5;

public class Main {

    public static void main(String[] args) {
        try {
            UserAuthMD5Application.run(args);
        } catch (Exception e) {
            System.err.println(e.getMessage());
        }
    }
}
