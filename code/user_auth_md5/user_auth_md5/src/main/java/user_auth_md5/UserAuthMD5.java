/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package user_auth_md5;

import com.sun.corba.se.impl.orbutil.ObjectUtility;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author Artur Muiria
 */
public class UserAuthMD5 {

    public static HashMap<String, String> usersInfo = new HashMap<String, String>();
    public static HashMap<String, String> usersAuth = new HashMap<String, String>();

    public UserAuthMD5() throws IOException {
        this.usersInfo = md5();
        this.usersAuth = newUsers();
    }

    HashMap<String, String> md5() throws FileNotFoundException, IOException {
        HashMap<String, String> usersInfo = new HashMap<>();
        try (BufferedReader br = new BufferedReader(new FileReader("usersInfo.txt"))) {
            String line;
            while ((line = br.readLine()) != null) {
                String[] values = line.split(",");
                usersInfo.put(values[0], values[1]);
//                usersAuth.put(values[0], null);
//                this.usersAuth.put(values[0], null);
            }
        } catch (IOException e) {

        }

//        usersInfo.put("Joao", "827ccb0eea8a706c4c34a16891f84e7b"); //Pass=12345
//        usersInfo.put("Maria", "5f4dcc3b5aa765d61d8327deb882cf99"); //Pass=password
//        usersInfo.put("RCOMP", "ea6b2efbdd4255a9f1b3bbc6399b58f4"); //Pass=2019
        return usersInfo;
    }

    HashMap<String, String> newUsers() throws FileNotFoundException, IOException {
        HashMap<String, String> usersAuth = new HashMap<>();
        for (Map.Entry<String, String> entry : usersInfo.entrySet()) {
            String key = entry.getKey();
            Object value = entry.getValue();
            usersAuth.put(key, null);
        }
        return usersAuth;
    }

    static Boolean exist(String username) {
        for (Map.Entry<String, String> entry : usersInfo.entrySet()) {
            String key = entry.getKey();
            Object value = entry.getValue();
            if (key.equalsIgnoreCase(username)) {
                return true;
            }
        }
        return false;
    }

    String generateAuthToken(String username) {

        SecureRandom random = new SecureRandom();
        byte bytes[] = new byte[20];
        random.nextBytes(bytes);
        String token = bytes.toString();

        for (Map.Entry<String, String> entry : usersAuth.entrySet()) {
            String key = entry.getKey();
            Object value = entry.getValue();
            if (key.equalsIgnoreCase(username)) {
                if (value == null) {
                    entry.setValue(token);
                }
            }
        }

        return token;
    }

    String generateAuthTokenNouser(String username) {

        SecureRandom random = new SecureRandom();
        byte bytes[] = new byte[20];
        random.nextBytes(bytes);
        String token = bytes.toString();

        return token;
    }

    void cheackUser(String username, String password, String AuthToken) throws NoSuchAlgorithmException {
        String validationToken = null;
        String md5Password = null;
        String storedmd5Password = null;

        //get stored md5 password from user
        for (Map.Entry<String, String> entry : usersInfo.entrySet()) {
            String key = entry.getKey();
            Object value = entry.getValue();
            if (key.equalsIgnoreCase(username)) {
                storedmd5Password = value.toString();
            }
        }

        //get stored token from user
        for (Map.Entry<String, String> entry : usersAuth.entrySet()) {
            String key = entry.getKey();
            Object value = entry.getValue();
            if (key.equalsIgnoreCase(username)) {
                if (value != null) {
                    validationToken = value.toString();
                    entry.setValue(null);
                }
            }
        }

        //password to md5
        MessageDigest objMD5 = MessageDigest.getInstance("MD5");
        byte[] bytMD5 = objMD5.digest(password.getBytes());
        BigInteger intNumMD5 = new BigInteger(1, bytMD5);
        String hcMD5 = intNumMD5.toString(16);
        while (hcMD5.length() < 32) {
            hcMD5 = "0" + hcMD5;
        }
        md5Password = hcMD5;

        if (validationToken != null) {
            if (md5Password.equalsIgnoreCase(storedmd5Password) && validationToken.equalsIgnoreCase(AuthToken)) {
                System.out.println("\nAccepted");
            } else {
                System.out.println("\nUnaccepted");
            }
        } else {
            System.out.println("\nUnaccepted");
        }
    }

}