/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package isep.eapli.rcomp5;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.util.HashSet;

/**
 *
 * @author marco
 */
public class QueueApplication {
    
    private static final int PORT = 31201;
    private static final String BCAST_ADDR = "255.255.255.255";
    private static HashSet<InetAddress> peersList = new HashSet<>();

    public static synchronized void addIP(InetAddress ip) { 
        peersList.add(ip);
    }

    public static synchronized void remIP(InetAddress ip) { 
        peersList.remove(ip);
    }

    public static synchronized void printIPs() { 
        for(InetAddress ip: peersList) {
            System.out.print(" " + ip.getHostAddress());
        }
    }

    public static synchronized void sendToAll(DatagramSocket s, DatagramPacket p) throws Exception { 
        for(InetAddress ip: peersList) {
            p.setAddress(ip);
            s.send(p);
        }
    }

    static InetAddress bcastAddress;
    static DatagramSocket sock;

    public static void run(String args[]) throws Exception {
        
        String nick, frase;
        byte[] data = new byte[300];
        byte[] fraseData;
        DatagramPacket udpPacket;

        try { 
            sock = new DatagramSocket(PORT); 
        } catch(IOException ex) {
            System.out.println("Failed to open local port");
            System.exit(1); 
        }

        BufferedReader in = new BufferedReader(new InputStreamReader(System.in));

        bcastAddress=InetAddress.getByName(BCAST_ADDR);
        sock.setBroadcast(true);
        data[0]=1;
        udpPacket = new DatagramPacket(data, 1, bcastAddress, PORT);
        sock.send(udpPacket);

        Thread udpReceiver = new Thread(new QueueRunnable(sock));
        udpReceiver.start();

        QueueUI ui=new QueueUI();
        while(true) { // handle user inputs
            frase=ui.runProgram();
            if(frase.compareTo("EXIT")==0) break;
            if(frase.compareTo("LIST")==0) {
                System.out.print("Active peers:");
                printIPs();
                System.out.println("");
            } else {
                fraseData = frase.getBytes();
                udpPacket.setData(fraseData);
                udpPacket.setLength(frase.length());
                sendToAll(sock,udpPacket);
            }
        }
        data[0]=0; // announce I'm leaving
        udpPacket.setData(data);
        udpPacket.setLength(1);
        sendToAll(sock,udpPacket);
        sock.close();
        udpReceiver.join(); // wait for the thread to end
    } 
}