/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package isep.eapli.rcomp5;

import java.util.Map;
import java.util.Scanner;

/**
 *
 * @author marco
 */
public class QueueUI {
    private static final QueueManager qMan = new QueueManager();
    private static final String MENU = ""
            + "==================================\n"
            + "               MENU\n"
            + "==================================\n";

    public synchronized String runProgram() {
        String qID, result;
        Scanner scanner = new Scanner(System.in);
        System.out.println(MENU + "1 - ENQUEUE\n2 - DEQUEUE\n3 - REMOVE QUEUE\n4 - LIST QUEUES\n5 - LIST ACTIVE PEERS\n0 - EXIT\n\nInsert Option: ");
        if (!scanner.hasNextInt()) {
            return runProgram();
        }

        int option = scanner.nextInt();

        switch (option) {
            case 1:
                System.out.printf("Input Queue Identifier: ");
                qID=scanner.nextLine();
                scanner.nextLine();
                System.out.printf("Input Queue element: ");
                String element=scanner.nextLine();
                result = String.valueOf(qMan.enqueue(qID, element));
                System.out.printf("%s%n",result);
                return result;
            case 2:
                System.out.println("Input Queue identifier: ");
                qID=scanner.nextLine();
                result = qMan.dequeue(qID);
                System.out.printf("%s%n",result);
                return result;
            case 3:
                System.out.println("Input Queue identifier: ");
                qID=scanner.nextLine();
                result=qMan.remove(qID);
                System.out.printf("%s%n",result);
                return result;
            case 4:
                Map<String,Integer> map=qMan.list();
                result = "";
                for(Map.Entry<String,Integer> entry:map.entrySet()) {
                    result+="Queue "+entry.getKey()+" - Size: "+entry.getValue()+";\n";
                }
                System.out.printf("%s%n",result);
                return result;
            case 5:
                return "LIST";
            case 0:
                return "EXIT";
            default:
                return runProgram();
        }
    }    
}
