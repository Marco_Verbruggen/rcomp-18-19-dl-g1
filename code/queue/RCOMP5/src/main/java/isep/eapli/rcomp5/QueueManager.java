/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package isep.eapli.rcomp5;

import java.util.HashMap;
import java.util.Map;
import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;

/**
 *
 * @author marco
 */
public class QueueManager {
    
    private Map<String,Queue> queueMap;
    
    public QueueManager() {
        queueMap=new HashMap<>();
    }
    
    public int enqueue(String qID, String string) {
        if(queueMap.containsKey(qID) && string.getBytes().length<=400) {
            queueMap.get(qID).add(string);
        } else if(!queueMap.containsKey(qID) && qID.getBytes().length<=80 && string.getBytes().length<=400){
            Queue q = new ConcurrentLinkedQueue();
            q.offer(string);
            queueMap.put(qID, q);
        } else {
            throw new IllegalArgumentException("Size Constraints Violated - "
                    + "queue identifier should be 80 or less characters long, "
                    + "queue element should be 400 bytes or less.");
        }
        return queueMap.get(qID).size();
    }
    
    public String dequeue(String qID) {
        if(queueMap.containsKey(qID)) {
            return (String)queueMap.get(qID).poll();
        } else {
            return String.format("Invalid Queue Identifier - this queue does not exist.");
        }
    }
    
    public String remove(String qID) {
        if(queueMap.containsKey(qID)) {
            queueMap.remove(qID);
            return String.format("Queue Successfully Removed");
        } else  {
            return String.format("Invalid Queue Identifier - this queue does not exist.");
        }
    }
    
    public Map<String,Integer> list() {
        Map<String,Integer> retMap = new HashMap<>();
        for(Map.Entry<String,Queue> entry:queueMap.entrySet()) {
            retMap.put(entry.getKey(), entry.getValue().size());
        }
        return retMap;
    }
    
}
