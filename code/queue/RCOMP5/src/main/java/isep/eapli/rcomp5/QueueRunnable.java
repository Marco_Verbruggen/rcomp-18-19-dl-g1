/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package isep.eapli.rcomp5;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;

/**
 *
 * @author marco
 */
public class QueueRunnable implements Runnable {    
    private DatagramSocket s;

    public QueueRunnable(DatagramSocket udp_s) { 
        s=udp_s;
    }

    public void run() {
        int i;
        byte[] data = new byte[300];
        String frase;
        DatagramPacket p;
        InetAddress currPeerAddress;

        p=new DatagramPacket(data, data.length);

        while(true) {
            p.setLength(data.length);
            try { 
                s.receive(p); 
            } catch(IOException ex) { 
                return; 
            }
            currPeerAddress=p.getAddress();

            if(data[0]==1) { // peer start
                QueueApplication.addIP(p.getAddress());
                try { 
                    s.send(p); 
                } catch(IOException ex) { 
                    return; 
                }
            } else if(data[0]==0) { // peer exit
                QueueApplication.remIP(p.getAddress());
            } else {		// chat message
                frase = new String( p.getData(), 0, p.getLength());
                System.out.println(frase);
            }
        }
    }    
}
