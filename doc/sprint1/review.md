RCOMP 2018-2019 Project - Sprint 1 review
=========================================
### Sprint master: 1160723 ###
(This file is to be created/edited by the sprint master only)
# 1. Sprint's backlog #

* T.1.1  Development of a structured cabling project for building A, and also encompassing the campus backbone. 
* T.1.2  Development of a structured cabling project for building B.
* T.1.3  Development of a structured cabling project for building C.
* T.1.4  Development of a structured cabling project for building D.
* T.1.5  Development of a structured cabling project for building E.
* T.1.6  Development of a structured cabling project for building F.
* T.1.7  Development of a structured cabling project for building G

# 2. Subtasks assessment #

## 2.1. 1170521 - Structured cable design for building A and backbone #
### Partially implemented with issues. ###
A better solution for the cable pathways could be designed.
## 2.2. 1161274 - Structured cable design for building B #
### Partially implemented with issues. ###
## 2.3. 1160723 - Structured cable design for building C #
### Partially implemented with issues. ###
Lack of equipment inventory.
## 2.4. 1170944 - Structured cable design for building D #
### Partially implemented with issues. ###
The assignment should be more clear.
We should've had more guidelines during the sprint.
There was lack of communication between the team members.
## 2.5. 1170677 - Structured cable design for building E #
### Partially implemented with issues. ###
Difficulty in measurments and positioning with precision.
## 2.6. 1170614 - Structured cable design for building F #
### Totally implemented with issues. ###
Cross Connectors could be better placed on both floors.
## 2.7. 1170623 - Structured cable design for building G #
### Partially implemented with issues. ###
Difficulty in some measurments aspects, lack of precision.
