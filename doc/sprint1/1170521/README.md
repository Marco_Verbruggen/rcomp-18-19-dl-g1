﻿RCOMP 2018-2019 Project - Sprint 1 - Member 1170521 folder
===========================================

# Building A
## Floor 0
![Floor 0](.\BUILDING-A-F0.png)

## Floor 1
![Floor 1](.\BUILDING-A-F1.png)

## Legenda

**BLUE CABLE** – Straight Through Copper Cable CAT7;  
**YELLOW CABLE** – Fiber Cable 10 Gigabit Ethernet;  
**PURPLE CIRCLE** – Access-Point;  
**RED CIRCLE** – Set of 3 Outlets;  
**PINK STAR** – Main Cross Connect e Intermediate Cross Connect;  
**GREEN RECTANGLE** - Horizontal Cross Connect;  
**ORANGE RECTANGLE** – Switch + Patch Panel;  
**BROWN CIRCLE** – Outlet for Access-Points;  

## Inventory

### 4 Access-Points 2.4GHzband channels and 802.11g:
1.	Channel 1;
2.	Channel 6;
3.	Channel 6;
4.	Channel 11;
### 4 Switches and Patch Panels:
* 2 Horizontal Cross Connected: Switch + 2 Patch Panel with 24 ports (1 for fiber and 1 for copper cables);
* 1 Main Cross Connected and Intermidiate Cross Connected: Switch + Patch Panel with 24 ports;
* 1 Consolidation Point: Switch + Patch Panel with 24 ports;
### 408 Outlets:
* 404 for End Nodes;
* 4 for the Access-Points;
### Floor Zero Cabling:
* 1124 meters of Straight Through Copper Cable CAT7;
* 86 meters of Multimode Optical Fiber Cable 10 Gigabit Ethernet;
### Floor One Cabling:
* 3456 meters of Straight Through Copper Cable CAT7;
* 102 meters of Multimode Optical Fiber Cable 10 Gigabit Ethernet;

* It was used Multimode Optical Fiber Cable 10 Gigabit Ethernet between intermidiate nodes because it uses 2 optical fibers to transmit in full-duplex, maximum cable length
depends on the fiber used, from 25 up to 400 meters.
* It was used Straight Through Copper Cable CAT7 between switches and outlets be


## Backbone
![Bacbone](.\BACKBONE.png)

For the backbone we used two monomode optical fiber cables per building connecting to the main cross-connect and 3 fiber cables making a top route (Building B-C-D-E) and another 3 making a bottom route (Buildings A-G-F-E) to ensure redundancy on our network.  
That means a total of 18 units and an amount of 4273 meters of cable. Monomode optical fiber cables for the backbone because it uses 2 optical fibers up to 10000 meters long, full-duplex.
