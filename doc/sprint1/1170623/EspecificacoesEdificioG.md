﻿## Especificações - Edifício G ##

## Inventário ##

# Fibra Ótica #

Todos os cabos de fibra ótica são monomodo 100GB (dado suportarem maior volume de dados transmitidos e serem imunes a difusão do sinal) de duas fibras (para permitir transmissão de dados em duplex).
O comprimento de cabos "calculados utilizados" nas linhas seguintes indica o comprimento calculado ser necessário para ligar todos os cabos. No entanto, este comprimento é ligeiramente superior ao verdadeiro comprimento utilizado, visto que o comprimento dos cabos seria menor quando ligados aos armários de telecomunicações.
O "Valor necessário" indica um número arredondado acima do valor calculado, mais um comprimento de cabo extra para ser usado no caso de erros de cálculos, erros de instalação que desperdicem cabo, ou em manutenção posterior.

* Piso 0: 944m calculados utilizados. Valor necessário: 1000m;
* Piso 1: 1149m calculados utilizados. Valor necessário: 1200m;
* Total: 2093m calculados utilizados. Valor neecessário: 2200m;

# Cabos de Cobre #

Todos os cabos de cobre são CAT7, devido a suportar maior volume de dados.
O comprimento "calculado utilizado" e "necessário" tem o mesmo significado que a fibra ótica.

* Piso 0: 2013.48m calculados utilizados. Valor necessário: 2050m;
* Piso 1: 2165.7m calculados utilizados. Valor necessário: 2200m;
* Total: 4179.18m calculados utilizados. Valor necessário: 4250m;

# Armários de Telecomunicações 6U #

* Piso 0: 2;
* Piso 1: 1;
* Total: 3;

# Armários de Telecomunicações 18U #

* Piso 0: 3;
* Piso 1: 0;
* Total: 3;

# Armários de Telecomunicações 22U #

* Piso 0: 0;
* Piso 1: 3;
* Total: 3;

# Switches #

* Piso 0: 14;
* Piso 1: 16;
* Total: 30;

# Patch Pannels de Cabos de Cobre #

* Piso 0: 9;
* Piso 1: 12;
* Total: 21;

# Patch Pannels de Cabos de Fibra Ótica #

* Piso 0: 5;
* Piso 1: 4;
* Total: 9;

# Outlets Ethernet de Cabos de Cobre #

As outlets foram dispostas nas salas de modo a haver sempre uma outlet a 3 metros ou menos de qualquer ponto numa sala. Cada sala tem um mínimo de duas outlets, mais duas por cada 10 metros quadrados de área da sala.

* Piso 0: 180;
* Piso 1: 192;
* Total: 378;

# Outlets Ethernet de Fibra Ótica #

As outlets de fibra ótica são colocadas em posições específicas e destinadas especificamente a pontos de acesso de WiFi.

* Piso 0: 3;
* Piso 1: 3;
* Total: 6;

# Wireless Access Points #

Access Points Wireless de banda de canais de 5GHz, por terem um maior número de canais que faz mais fácil evitar interferências de sinal.

* Piso 0: 3;
* Piso 1: 3;
* Total: 6;

## Layout do Equipamento ##

# Conneções entre Equipamentos #

Para dar redundância à rede, as coneções entre o IC e os HCs têm dois cabos paralelos cada, os HCs têm dois cabos paralelos a uni-los um ao outro e outro par a unir cada HC a cada CP no seu piso, e cada CP tem dois cabos a uni-lo a cada outro CP no mesmo piso. As ligações de cabos de cobre (dos CPs às outlets) e as ligações dos CPs às outlets de fibra destinadas a WiFi Access Points têm apenas um cabo cada.

# Intermediate Cross-Connect #

O IC consiste de um armário de telecomunicações de 6U, que contém um patch pannel de fibra ótica com 12 ports e uma switch apropriada para esse patch pannel. Incluindo espaço para uma expansão de 100%, o espaço ocupado é de 4U, portanto utiliza-se o armário standard mais pequeno de 6U.
Os 7 cabos de fibra ótica e os 4 cabos do intermediate backbone ligam-se à switch através deste patch pannel.

# Horizontal Cross-Connects #

O HC de cada piso consiste de um armário de telecomunicações de 6U, que é igual ao IC.
Os 2 cabos do itermediate backbone que ligam cada HC ao IC, assim como os 6 cabos que ligam cada HC aos CPs do seu piso, e os dois cabos que ligam os HCs um ao outro ligam-se à switch atravès do patch pannel de fibra ótica.

# Consolidation Points #

Os CPs consistem todos de armários de telecomunicações standard de 18 ou 22U, dependendo da sala. Estes tamanhos acomodam o equipamento necessário e espaço para expansão de 100%.
* Salas G0.2, G0.5 e G0.9: O CP é um armário de telecomunicações de 18U e contém um patch pannel de fibra ótica de 12 ports, 3 patch pannels de cabos de cobre de 24 ports, e 4 switches de 24 ports. Os dois cabos que ligam o CP ao HC e os 4 que o ligam aos outros 2 CPs do piso ligam-se a uma switch através do patch pannel de fibra, e os cabos de cobre que se ligam às outlets das salas servidas pelo CP. O CP da sala G0.2 serve as salas G0.1, G0.2, G0.3 e G0.4, e tem um cabo de fibra adicional destinado a um dos WiFi Access Points. O CP da sala G0.5 serve as salas G0.5, G0.6 e G0.7, e tem dois cabos de fibra adicionais ligados destinados a dois WiFi Access points. O CP na sala G0.9 serve a sala G0.8, G0.9, G0.10 e G0.11. As switches de cada CP devem estar todas ligadas.
* Salas G1.2, G1.5 e G1.11: O CP é igual ao primeiro CP descrito, mas com um patch pannel de cabos de cobre de 24 ports e uma switch de 24 ports adicionais. Cada CP tem um único cabo de fibra ligado a um WiFi Access Point, mais os cabos de fibra que os ligam entre si e ao HC do piso 1, como descrito para os CPs do piso 0. A switch e path pannel adicional aumenta o tamanho necessário do armário para 22U. O CP da sala G1.2 serve as salas G1.2, G1.3, G1.4 e G1.15. O CP da sala G1.5 serve as salas G1.5, G1.6, G1.7 e G1.8. O CP da sala G1.11 serve as salas G1.9, G1.10, G1.11, G1.12, G1.13 e G1.14.

## Planta das Pathways para Cabos e Colocação das Outlets e Armários de Telecomunicação ##

Nota: Cada ícone de outlet pode representar mais que uma outlet, ler anotações. (GX.Y.Z - X representa o piso, Y a sala, Z o número da outlet. GWX.Y.Z representa uma outlet para utilização de wireless access point). Linhas coloridas representam cable pathways conforme a legenda, e não cabos individuais;

[MapaCabos](MapaCabos.png)