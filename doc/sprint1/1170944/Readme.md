# Notas
Todas as informações sobre a planificação da cablagem encontram-se nos 3 projetos de packet tracer; Um deles tem a vista fisica do piso 0 do edificio e os outros 2 do piso 1; Devido a problemas de desempenho do packet tracer, tive de dividir a planificação em 3 projetos separados.  
É também importante referir algumas coisas : Cabos que ligam entre switches são crossover; O packet tracer não têm a opção de por outlets de duas tomadas de rede, por isso vamos assumir que os outlets desenhados no projeto não são de parede, mas sim de chão e que cada um tem 2 tomadas de rede.
Para informações detalhadas sobre cada cabo, consultar o ficheiro excel disponibilizado ou o projeto de packet tracer.


# Legendas
> Cabo a amarelo : Cabo que passa pelo chão<;
> Cabo a preto : Cabo que passa pelo teto.

# Building D - FLOOR0
> Este piso tem 11 salas; O IC tem 1 router, um switch central e, nesse mesmo rack, encontra-se o switch dessa sala e um patch pannel para ligar aos outlets. As decisões das quantidades de outlets a usar está justificada no ficheiro de medições das áreas das salas. As restantes salas têm todas o seu próprio switch e o seu patch pannel; No total, neste edificio foram usados 274,2 metros de cabo crossover CAT7, 1236,06 de cabo straight CAT7
![area_d_f0.png](area_d_f0.png)

# Building D - FLOOR1
> Este piso tem 24 salas; O IC tem 1 router, 2 Switches centrais (pois um não tinha portas suficientes para agregar os switches de todas as salas) mais o switch da sala em que está incluido e um patch pannel para ligar aos outlets. As decisões das quantidades de outlets a usar está justificada no ficheiro de medições das áreas das salas. As restantes salas têm todas o seu próprio switch e o seu patch pannel; No total, neste edificio foram usados 2075,5 metros de cabo crossover CAT7, 1224,8 de cabo straight CAT7
![area_d_f1.png](area_d_f1.png)


A nivel de Access Points, são usados 4 por piso pois há várias paredes que podem cortar sinal e não nos dizem o total de pessoas que podem usar o WI-FI, então achei por bem reforçar o sinal.

Os APs devem funcionar da seguinte forma:
1: Canal 1
2: Canal 6
3: Canal 6
4: Canal 11
