RCOMP 2018-2019 Project - Sprint 1 - Member 1170614 folder
===========================================

Neste andar os cabos vão pelo teto falso, descendo até aos outlets pelas paredes, exceto nos outlets que já se encontram no chão, estes os cabos passam por cima do chão.
Salas F0.1 até à F0.5: 6,20m x 7,77m = 48,2m   10 outlets  , 3 encontram se no chão.
•	F0.5 tem um Consolidation Point que contém 3 switch(24 portas) e 4 patch panel(24 portas), 3 com ligações de cobre e 1 com ligações de fibra.
•	Cada sala utiliza em média 118m de cabo de cobre (CAT7), sendo no total                     (118 x 5) + 24m + 18m + 12m + 6m= 650m .
•	A Consolidation Point da sala F0.5 está ligada ao HC da sala F0.8 por cabo de fibra, por dois caminhos, para haver redundância.
•	A CP também se encontra ligada aos Acess-Points que se encontram na Área em comum.
•	É utilizado 60m de cabo de fibra monomode de 100Gb.
Salas F0.6 e F0.7: 8,47m x 5,50m = 46,58m  10 outlets, 3 encontram se no chão.
•	F0.7 tem um WAP (Access Point) no teto, o qual abrange a zona de trabalho toda.
•	Cada sala utiliza em média 100m de cabo de cobre (CAT7), sendo no total       	         100 x 2 = 200m.
Sala F0.8: 7,91m x 7,67m = 60,67m  12 outlets, 4 encontram se no chão.
•	F0.8 tem um Intermediate cross-connect e tem um Horizontal cross-connect  ligados com cabo de fibra monomode de 100Gb(1m), utiliza 134m de cabo de cobre (CAT7) e contém 3 switch(24 portas) e 3 patch panel(24 portas), 2 com ligações de cobre e 1 com ligações de fibra.
Sala F0.9: 7,69m x 6,15m = 47,29m  10 outlets, 3 encontram se no chão.
•	F0.9 utiliza em média 110m de cabo de cobre (CAT7).


Salas F0.10 até à F0.11 = 5,63m x 7,69m = 43,29m  8 outlets, 2 encontram se no chão.
•	F0.10 e F0.11 utilizam em média 107m de cabo de cobre(CAT7), no total                      107 x 3 = 321m.
No total o andar utiliza em cabo de cobre (CAT7) 321m + 200m + 650m + 134m + 110m + 100m(ligações entre HC e CPs, e outlet de Wi-Fi) = 1515m, 60m de cabo de fibra monomode 100Gb,115 outlets,6 switch, 7 patch panels e 3 WAP(Access Points), 2 destes encontram-se na área comum ligados ao CP mais próximo(F0.5) e 1 HC para obedecer a regra de 1 HC por 1000m2, como este andar tem 2400m2 mas só são utilizados 900m2 foi colocado só 1 Horizontal cross-connect.



Neste andar os cabos vão pelo teto falso, descendo até aos outlets pelas paredes, exceto nos outlets que já se encontram no chão, estes os cabos pelo teto falso do andar de baixo, passando assim por baixo do chão.
Salas F1.1 até à F1.10: 6,0m x 7,72m = 46,2m   10 outlets, 3/4 deles encontram se no chão.
•	F1.3 tem um Horizontal cross-connect que contém 3 switch e 4 patch panel(24 portas), 3 de ligação de cobre e 1 de ligação de fibra, esta HC encontra-se ligada aos dois HC que se encontram na sala F1.17 e F1.21, para haver maior redundância.
•	Cada sala utiliza em média 140m de cabo de cobre (CAT7), sendo no total                   140 x 10 = 1400m, a sala F1.03 utiliza 100m de fibra monomode de 100Gb.
Salas F1.11 e F1.16: 8,22m x 5,50m = 45,21m  10 outlets, 2 deles encontram se no chão.
•	F1.12 e F1.16 tem em cada sala um WAP (Access Point) no teto, o qual abrange uma parte da zona de trabalho.
•	Cada sala utiliza em média 109m de cabo de cobre (CAT7), sendo no total                   109 x 6 = 654m.
Sala F1.17, F1.19 até à F1.23: 7,66m x 7,59m = 58,14m  12 outlets, 3 deles encontram se no chão.
•	F1.17 e F1.22 tem um Horizontal cross-connect ligado entre si e ao HC da sala F1.03 por cabo de fibra monomode 100Gb, para haver maior redundância.
•	F1.17 e F1.22 contém 5 switch e 7 patch panel, 6 de ligações de cobre e 1 de ligação de fibra, utilizando 100m de cabo de fibra.
•	F1.21 tem um WAP (Access Point) no teto, o qual abrange uma parte da zona de trabalho.
•	Cada sala utiliza em média 140m de cabo de cobre (CAT7), sendo no total      	        140 x 6 = 840m.
Sala F1.18: 5,63m x 7,69m = 45,77m  10 outlets, 3 deles encontram se no chão.
•	F1.18 e utiliza em média 95m de cabo de cobre (CAT7).
Salas F1.24 = 9,84m x 7,61m = 74,88m  16 outlets, 5 deles encontram se no chão.
•	F1.24 utiliza em média 150m de cabo de cobre(CAT7).
No total o andar utiliza em cabo de cobre (CAT7) 1400m + 654m + 840m + 95m+ 150m + 140m(ligações entre HC e CPs, e outlet de Wi-Fi) = 3279m,200m de cabo de fibra monomode 100Gb,258 outlets,13 switch, 20 patch panels, 3 WAP(Access Points) e 3 HC para obedecer a regra de 1 HC por 1000m2, como este andar tem 2400m2 foram utilizados 3 Horizontal cross-connects.
