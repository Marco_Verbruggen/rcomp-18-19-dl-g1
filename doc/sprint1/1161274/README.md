RCOMP 2018-2019 Project - Sprint 1 - Member 5555555 folder
===========================================
(This folder is to be created/edited by the team member 5555555 only)

#### This is just an example for a team member with number 5555555 ####
### Each member should create a folder similar to this, matching his/her number. ###
The owner of this folder (member number 5555555) will commit here all the outcomes (results/artifacts/products)		       of his/her work during sprint 1. This may encompass any kind of standard file types.

# Building B

## Floor 0

![Floor 0](BUILDING-B-F0.png)



## Floor 1

![Floor 1](BUILDING-B-F1.png)



## Legenda


**CABO PRETO** – Straight Through Copper Cable CAT7;

**CABO PRETO** – Cross Over Copper Cable CAT7;

**CIRCULO VERMELHO** – Outlet;

**RECTANGULO VERMELHO** – Intermediate Cross Connect;

**RETANGULO VERDE** - Main Cross Connected and Horizontal Cross Connect;

**RETANGULO CASTANHO** – Switch + Patch Panel;

**RETANGULO LARANJA** – Access-Point;


## Inventário


### 7 Switches and Patch Panels:

* 5 Horizontal Cross Connected;

* 1 Main Cross Connected and Intermediate Cross Connected;

### Area e Outlets:

* Balcão de Atendimento - 21.15m² - 6 Outlets
* B0.2-  73.73m² - 10 Outlets
* B0.3-  67.92m² - 10 Outlets
* B0.4-  65.75m² - 8 Outlets
* B0.5-  171.39m² - 18 Outlets
* B1.1-  23.40m² - 4 Outlets
* B1.2-  38.78m² - 6 Outlets
* B1.3-  71.27m² - 8 Outlets
* B1.4-  69.19m² - 9 Outlets
* B1.5-  75.55m² - 6 Outlets
* B1.6-  74.17m² - 6 Outlets
* B1.7-  73.87m² - 6 Outlets
* B1.8-  62.49m² - 8 Outlets
* B1.9-  60.77m² - 8 Outlets

* 4 for the Access-Points;
