RCOMP 2018-2019 Project - Sprint 1 planning
===========================================
### Sprint master: 1160723 ###
(This file is to be created/edited by the sprint master only)
# 1. Sprint's backlog #
* T.1.1  Development of a structured cabling project for building A, and also encompassing the campus backbone. 
* T.1.2  Development of a structured cabling project for building B.
* T.1.3  Development of a structured cabling project for building C.
* T.1.4  Development of a structured cabling project for building D.
* T.1.5  Development of a structured cabling project for building E.
* T.1.6  Development of a structured cabling project for building F.
* T.1.7  Development of a structured cabling project for building G

# 2. Technical decisions and coordination #

### The cable used between cross connector is optical fiber. ###
### The cable used between outlets is Cooper Cable CAT7. ###
### From each building to the main one there is two cables (Redundancy). ###

# 3. Subtasks assignment #
(For each team member (sprint master included), the description of the assigned subtask in sprint 1)

 * 3.1. 1170521 - Structured cable design for building A and backbone
 * 3.2. 1161274 - Structured cable design for building B
 * 3.3. 1160723 - Structured cable design for building C
 * 3.4. 1170944 - Structured cable design for building D
 * 3.5. 1170677 - Structured cable design for building E
 * 3.6. 1170614 - Structured cable design for building F
 * 3.7. 1170623 - Structured cable design for building G
