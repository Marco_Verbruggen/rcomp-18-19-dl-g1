
# Inventário


## Piso 1

![](piso1medido.png)

A sala E0.11 contem o Intermediate cross-connect.
A sala E0.11 tem um Horizontal cross-connect e a sala E0.1 tem um Consolidation Point. O Horizontal cross-connect é composto por um Switch e um patch panel.
Número de Outlets: 116
Isto porque os outlets foram distribuídos de modo a que houvesse 2 outlets a cada 10 m2 e em nenhuma zona se de cada sala se pudesse estar a mais de 3 m de um outlet.
Número de Access Points: 2

Nas Salas E0.1 , E0.2, E0.3 , E0.4, E0.5 ,  E0.9, E0.10 e E0.11 para ligar os outlets ao Consolidation Point ou HC é usado em média (0,75 + 3,79 + 1,97 + 5,15 + 6,69 + 8,16 + 9,49 + 3,38 + 4,57 + 5,97) ou seja 49,92 metros de cabo de cobre (CAT7) por sala. Totalizando assim 399,36 metros de cabo para estas salas.
Nas Salas E0.6 e E0.7 para ligar os outlets ao Consolidation Point são usados em média (1,30 + 4,08 + 5,4 + 6,86 + 8,23 + 9,96 + 2,62 + 4,22 + 5,56 + 6,86) ou seja 16,82 metros de cabo por sala. Totalizando assim 22,64 metros de cabo de cobre (CAT7) para estas salas.
Na sala E0.7, e no cnetroa da área aberta são ainda usados mais 51,37 m de cabo de cobre (CAT7) para ligar aos o HC aos Access Points garantindo assim que há pelo menos 1 Access Point por 1000 m2 e não há mais que 200 conexões a cada Access Point.
Na Sala E0.8 para ligar os outlets ao HC são usados em média (1,25 + 2,89 + 4,93 + 6,69 + 8,43 + 9,91 + 11,71 + 13,4 + 10,35 + 12,02 + 13,99 + 15,54 + 11,53 + 13,29 + 15,14 + 17,02) ou seja 168,09 metros de cabo de cobre (CAT7).

Do IC até ao HC da sala E0.10 são usados 1,52 metros de cabo e mais 1,52 metros de cabo para aumentar a probabilidade de pelo menos um estar operacional. Este cabo secundário, tem o mesmo percurso unicamente porque o IC e HC em questão estão demasiado próximos para que seja vantajoso ter um percurso diferente. Desse HC aos respetivos CP’s são usados 71,44 metros de cabo de fibra monomd de 100 Gigabytes.
Do IC até ao CP da sala E0.1 são usados 50,41 metros de cabo de fibra monomd de 100 Gigabytes.

__Neste Piso foram usados 896,82 metros de cabo.__

## Piso 2

![](piso2medido.png)

As salas E1.5, E1.20 e E1.13 tem um Horizontal cross-connect que é composto por um Switch e um patch panel.
Número de Outlets : 282
Isto porque os outlets foram distribuídos de modo a que houvesse 2 outlets a cada 10 m2 e em nenhuma zona se de cada sala se pudesse estar a mais de 3 m de um outlet.
Número de Access Points :3
Nas Salas E1.17 , E1.18, E1.19, E1.20, E1.21 ,  E1.22 e E1.23 para ligar os outlets ao HC de cada sala é usado em média (1,3+1,61+3,22+4,83+2,91+4,52+6,13+7,74+4,52+6,13+7,74+9,15+6,13+7,74+9,15+10,76) ou seja 93,58 metros de cabo de cobre (CAT7) por sala. Totalizando assim 561,48 metros de cabo para estas salas.
Nas Salas E1.1, E1.2, E1.3, E1.4, E1.5, E1.6, E1.7, E1.8, E1.9, E1.10 e E1.18 para ligar os outlets ao Horizontal Cross Connect é usado em média (1,56+4,56+5,79+7,02+8,25+9,48+2,79+4,02+5,58+6,81) ou seja 55,86 metros de cabo de cobre (CAT7) por sala. Totalizando assim 614,46 metros de cabo para estas salas.

Nas Salas E1.11, E1.12, E1.13, E1.14, E1.15 e E1.16para ligar os outlets ao HC de cada sala é usado em média (1,56+4,05+5,55+7,05+8,55+10,05+3,06+4,56+6,06+7,56) ou seja 58,05 metros de cabo de cobre (CAT7) por sala. Totalizando assim 348,3 metros de cabo para estas salas.
Na sala E1.12, E1.16 e E1.20 são ainda usados mais 16,96 m de cabo de cobre (CAT7) para ligar aos o HC aos Access Points garantindo assim que há pelo menos 1 Access Point por 1000 m2 e não há mais que 200 conexões a cada Access Point.
Na Sala E1.24 para ligar os outlets ao HC são usados em média (1,34 + 3,25 + 5,16 + 7,07 + 3,25 + 5,16 + 7,07 + 8,98 + 5,16 + 7,07 + 8,98 + 10,89 + 7,07 + 8,98 + 10,89 +12,8) ou seja 113,12 metros de cabo de fibra monomd de 100 Gigabytes.

Do floor cable passageway até ao HC da sala E1.5 são usados 13,08 metros de cabo e mais 13,08 metros de cabo para aumentar a probabilidade de pelo menos um estar operacional. Este cabo secundário, tem o mesmo percurso unicamente porque o IC e HC em questão estão demasiado próximos para que seja vantajoso ter um percurso diferente. Este é um cabo de fibra monomd de 100 Gigabytes.
Do floor cable passageway até ao HC da sala E1.13 são usados 5,78 metros de cabo e mais 5,78 metros de cabo para aumentar a probabilidade de pelo menos um estar operacional. Este cabo secundário, tem o mesmo percurso unicamente porque o IC e HC em questão estão demasiado próximos para que seja vantajoso ter um percurso diferente. Este é um cabo de fibra monomd de 100 Gigabytes.
Do floor cable passageway até ao HC da sala E1.20 são usados 2,38 metros de cabo e mais 2,38 metros de cabo para aumentar a probabilidade de pelo menos um estar operacional. Este cabo secundário, tem o mesmo percurso unicamente porque o IC e HC em questão estão demasiado próximos para que seja vantajoso ter um percurso diferente. Este é um cabo de fibra monomd de 100 Gigabytes.

__Neste Piso foram usados 2209,54 metros de cabo.__
