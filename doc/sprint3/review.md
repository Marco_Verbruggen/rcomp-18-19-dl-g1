RCOMP 2018-2019 Project - Sprint 3 review
=========================================
### Sprint master: 1170944 ###
# 1. Sprint's backlog #
In this sprint, we gave the sprint2 work, layer 3 connectivity. Now we have one router per building, inter-vlan routing, VOIP phones working, DHCP and internet connectivity
# 2. Subtasks assessment #
One by one, each team member presents his/her outcomes to the team, the team assesses 		the accomplishment of the subtask backlog.
The subtask backlog accomplishment can be assessed as one of:

  * Totally implemented with no issues
  * Totally implemented with issues
  * Partially implemented with no issues
  * Partially implemented with issues

For the last three cases, a text description of what has not been implemented and present issues must be added.
Unimplemented features and issues solving is assigned to the same member on the next sprint.

(Examples)
## 2.1. 1170521 - IPv4 addressing and routing configurations for building A and simulating the internet connectivity #
### Totally implemented with no issues. ###
## 2.2. 1161274 - IPv4 addressing and routing configurations for building B and joining all the other buildings into one simulation #
### Totally implemented with issues. ###
## 2.3. 1170944 - IPv4 addressing and routing configurations for building D #
### Totally implemented with issues ###
## 2.4. 1170677 - IPv4 addressing and routing configurations for building E #
### Totally implemented with issues ###
This student found dificulties on addressing ip's to the DHCP's pools.
## 2.5. 1170614 - IPv4 addressing and routing configurations for building F #
### Totally implemented with issues. ###
## 2.6. 1170623 - IPv4 addressing and routing configurations for building G #
### Totally implemented with no issues. ###
Had some issues to start, but then found the work easy.
