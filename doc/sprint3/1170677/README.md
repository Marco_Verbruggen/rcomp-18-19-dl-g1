RCOMP 2018-2019 Project - Sprint 3 - Member 1170677 folder
===========================================

## Router BuildingE_IC

!
version 12.4
no service timestamps log datetime msec
no service timestamps debug datetime msec
no service password-encryption
!
hostname Router_BuildingE
!
!
!
!
ip dhcp excluded-address 172.23.110.64 //DMZ
ip dhcp excluded-address 172.23.110.0 //FLOOR0
ip dhcp excluded-address 172.23.109.1 //FLOOR1
ip dhcp excluded-address 172.23.108.1 //WIFI
ip dhcp excluded-address 172.23.109.128 //VOIP
!
ip dhcp pool FLOOR1
 network 172.29.106.0 255.255.255.128
 default-router 172.23.109.1
 dns-server 8.8.8.8
 domain-name dei.isep.ipp.pt
ip dhcp pool VOIP
 network 172.29.106.128 255.255.255.128
 default-router 172.23.109.128
 option 150 ip 172.29.106.129
 dns-server 8.8.8.8
 domain-name dei.isep.ipp.pt
ip dhcp pool WIFI
 network 172.29.105.0 255.255.255.0
 default-router 172.23.108.1
 dns-server 8.8.8.8
 domain-name dei.isep.ipp.pt
ip dhcp pool FLOOR0
 network 172.29.107.0 255.255.255.192
 default-router 172.23.110.1
 dns-server 8.8.8.8
 domain-name dei.isep.ipp.pt
!
!
!
ip cef
no ipv6 cef
!
!
!
!
!
!
!
!
!
!
!
!
spanning-tree mode pvst
!
!
!
!
!
!
interface FastEthernet0/0
 ip address 172.23.96.1 255.255.255.192 //BackBone
 duplex auto
 speed auto
!
interface FastEthernet0/1
 no ip address
 duplex auto
 speed auto
!
interface FastEthernet0/1.511
 encapsulation dot1Q 511
 ip address 172.23.110.1 255.255.255.192
!
interface FastEthernet0/1.512
 encapsulation dot1Q 512
 ip address 172.23.109.1 255.255.255.128
!
interface FastEthernet0/1.513
 encapsulation dot1Q 513
 ip address 172.23.108.1 255.255.255.0
!
interface FastEthernet0/1.514
 encapsulation dot1Q 514
 ip address 172.23.110.64 255.255.255.240
!
interface FastEthernet0/1.515
 encapsulation dot1Q 515
 ip address 172.23.109.128 255.255.255.128
!
interface Vlan1
 no ip address
 shutdown
!
ip classless
ip route 0.0.0.0 0.0.0.0 172.29.96.129
!
ip flow-export version 9
!
!
!
!
!
!
!
!
dial-peer voice 1 voip
 destination-pattern 1..
 session target ipv4:172.29.96.1
!
dial-peer voice 2 voip
 destination-pattern 2..
 session target ipv4:172.29.96.2
!
dial-peer voice 3 voip
 destination-pattern 3..
 session target ipv4:172.29.96.3
!
dial-peer voice 4 voip
 destination-pattern 4..
 session target ipv4:172.29.96.4
!
dial-peer voice 6 voip
 destination-pattern 6..
 session target ipv4:172.29.96.6
!
dial-peer voice 7 voip
 destination-pattern 7..
 session target ipv4:172.29.96.7
!
telephony-service
 max-ephones 42
 max-dn 70
 auto assign 1 to 70
!
ephone-dn 1
 number 500
!
ephone-dn 2
 number 501
!
ephone-dn 3
 number 502
!
ephone-dn 4
 number 503
!
ephone-dn 5
 number 504
!
ephone-dn 6
 number 505
!
ephone-dn 7
 number 506
!
ephone-dn 8
 number 507
!
ephone-dn 9
 number 508
!
ephone-dn 10
 number 509
!
ephone-dn 11
 number 510
!
ephone-dn 12
 number 511
!
ephone-dn 13
 number 512
!
ephone-dn 14
 number 513
!
ephone-dn 15
 number 514
!
ephone-dn 16
 number 515
!
ephone-dn 17
 number 516
!
ephone-dn 18
 number 517
!
ephone-dn 19
 number 518
!
ephone-dn 20
 number 519
!
ephone-dn 21
 number 520
!
ephone-dn 22
 number 521
!
ephone-dn 23
 number 522
!
ephone-dn 24
 number 523
!
ephone-dn 25
 number 524
!
ephone-dn 26
 number 525
!
ephone-dn 27
 number 526
!
ephone-dn 28
 number 527
!
ephone-dn 29
 number 528
!
ephone-dn 30
 number 529
!
ephone-dn 31
 number 530
!
ephone-dn 32
 number 531
!
ephone-dn 33
 number 532
!
ephone-dn 34
 number 533
!
ephone-dn 35
 number 534
!
ephone-dn 36
 number 535
!
ephone-dn 37
 number 536
!
ephone-dn 38
 number 537
!
ephone-dn 39
 number 538
!
ephone-dn 40
 number 539
!
ephone-dn 41
 number 540
!
ephone-dn 42
 number 541
!
ephone-dn 43
 number 542
!
ephone-dn 44
 number 543
!
ephone-dn 45
 number 544
!
ephone-dn 46
 number 545
!
ephone-dn 47
 number 546
!
ephone-dn 48
 number 547
!
ephone-dn 49
 number 548
!
ephone-dn 50
 number 549
!
ephone-dn 51
 number 550
!
ephone-dn 52
 number 551
!
ephone-dn 53
 number 552
!
ephone-dn 54
 number 553
!
ephone-dn 55
 number 554
!
ephone-dn 56
 number 555
!
ephone-dn 57
 number 556
!
ephone-dn 58
 number 557
!
ephone-dn 59
 number 558
!
ephone-dn 60
 number 559
!
ephone-dn 61
 number 560
!
ephone-dn 62
 number 561
!
ephone-dn 63
 number 562
!
ephone-dn 64
 number 563
!
ephone-dn 65
 number 564
!
ephone-dn 66
 number 565
!
ephone-dn 67
 number 566
!
ephone-dn 68
 number 567
!
ephone-dn 69
 number 568
!
ephone-dn 70
 number 569
!
line con 0
!
line aux 0
!
line vty 0 4
 login
!
!
!
end

## Router MC

!
version 12.4
no service timestamps log datetime msec
no service timestamps debug datetime msec
no service password-encryption
!
hostname Router_A0_1
!
!
!
enable secret 5 $1$mERr$3HhIgMGBA/9qNmgzccuxv0
enable password 1234
!
!
!
!
!
!
no ip cef
no ipv6 cef
!
!
!
!
!
!
!
!
!
!
!
!
spanning-tree mode pvst
!
!
!
!
!
!
interface FastEthernet0/0
 no ip address
 duplex auto
 speed auto
!
interface FastEthernet0/1
 no ip address
 duplex auto
 speed auto
 shutdown
!
interface FastEthernet1/0
 no ip address
!
interface Vlan1
 no ip address
 shutdown
!
ip classless
!
ip flow-export version 9
!
!
!
!
!
!
!
!
line con 0
!
line aux 0
!
line vty 0 4
 password 12345
 login
!
!
!
end


## Tabela Edifício E

|Rede do Edifício|Broadcast|Rede|1º Válido|Máscara
|--------|--------|------|--------|------|-------|
|E - FLOOR0| 172.23.110.1| 172.23.110.1| 172.23.110.2|__/26__ 255.255.255.192|
|E - FLOOR1|172.23.109.1|172.23.109.1|172.23.109.2|__/25__  255.255.255.128|
|E - WIFI|172.23.108.1|172.23.108.1|172.23.108.2|__/24__  255.255.255.0|
|E - VOIP|172.23.109.128|172.29.106.128|172.23.109.129|__/25__  255.255.255.128|
|E - DMZ|172.23.110.64|172.29.107.64|172.23.110.65|__/28__  255.255.255.240|
