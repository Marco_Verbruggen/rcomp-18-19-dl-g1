RCOMP 2018-2019 Project - Sprint 3 - Member 1161274 folder
===========================================

# Configuration #
## Router BuildingB ##

Building B Network Table:  

|   VLAN  |      Rede      |      1º IP     |    Broadcast   | Mascara |
|:-------:|:--------------:|:--------------:|:--------------:|:-------:|
| Floor 0 |  172.29.98.128  |  172.29.98.129  | 172.29.98.191 |   /26   |
| Floor 1 | 172.29.98.0 | 172.29.98.1 | 172.29.98.127 |   /25   |
|   WIFI  |  172.29.97.128  |  172.29.97.129  | 172.29.97.255 |   /25   |
| DMZ     | 172.29.99.0  | 172.29.99.1  | 172.29.99.15  | /28     |
| VOIP    | 172.29.98.192   | 172.29.98.193   | 172.29.98.255 | /26     |


!
version 12.4
no service timestamps log datetime msec
no service timestamps debug datetime msec
no service password-encryption
!
hostname Router
!
!
!
!
ip dhcp excluded-address 172.29.97.129
ip dhcp excluded-address 172.29.98.1
ip dhcp excluded-address 172.29.98.129
ip dhcp excluded-address 172.29.98.193
ip dhcp excluded-address 172.29.99.1
!
ip dhcp pool WIFI
 network 172.28.97.128 255.255.255.128
 default-router 172.29.97.129
 dns-server 8.8.8.8
 domain-name dei.isep.ipp.pt
ip dhcp pool FLOOR1
 network 172.29.98.0 255.255.255.128
 default-router 172.29.98.1
 dns-server 8.8.8.8
 domain-name dei.isep.ipp.pt
ip dhcp pool FLOOR0
 network 172.29.98.128 255.255.255.192
 default-router 172.29.98.129
 dns-server 8.8.8.8
 domain-name dei.isep.ipp.pt
ip dhcp pool VOIP
 network 172.29.98.192 255.255.255.192
 default-router 172.29.98.193
 option 150 ip 172.29.98.193
 dns-server 8.8.8.8
 domain-name dei.isep.ipp.pt
ip dhcp pool DMZ
 network 172.29.99.0 255.255.255.240
 default-router 172.29.99.1
 dns-server 8.8.8.8
 domain-name dei.isep.ipp.pt
!
!
!
no ip cef
no ipv6 cef
!
!
!
!
!
!
!
!
!
!
!
!
spanning-tree mode pvst
!
!
!
!
!
!
interface FastEthernet0/0
 no ip address
 duplex auto
 speed auto
!
interface FastEthernet0/0.490
 encapsulation dot1Q 490
 ip address 172.29.96.1 255.255.255.192
!
interface FastEthernet0/1
 no ip address
 duplex auto
 speed auto
!
interface FastEthernet0/1.496
 encapsulation dot1Q 496
 ip address 172.29.98.129 255.255.255.192
!
interface FastEthernet0/1.497
 encapsulation dot1Q 497
 ip address 172.29.98.1 255.255.255.128
!
interface FastEthernet0/1.498
 encapsulation dot1Q 498
 ip address 172.29.97.129 255.255.255.128
!
interface FastEthernet0/1.499
 encapsulation dot1Q 499
 ip address 172.29.99.1 255.255.255.240
!
interface FastEthernet0/1.500
 encapsulation dot1Q 500
 ip address 172.29.98.193 255.255.255.192
!
interface FastEthernet1/0
 no ip address
 duplex auto
 speed auto
 shutdown
!
interface FastEthernet1/1
 no ip address
 duplex auto
 speed auto
 shutdown
!
interface Vlan1
 no ip address
 shutdown
!
ip classless
!
ip flow-export version 9
!
!
!
no cdp run
!
!
!
!
!
!
telephony-service
 max-ephones 30
 max-dn 30
 ip source-address 172.29.98.193 port 2000
 auto assign 1 to 30
!
ephone-dn 1
 number 200
!
ephone-dn 2
 number 201
!
ephone-dn 3
 number 202
!
ephone-dn 4
 number 203
!
ephone-dn 5
 number 204
!
ephone-dn 6
 number 205
!
ephone-dn 7
 number 206
!
ephone-dn 8
 number 207
!
ephone-dn 9
 number 208
!
ephone-dn 10
 number 209
!
ephone-dn 11
 number 210
!
ephone-dn 12
 number 211
!
ephone-dn 13
 number 212
!
ephone-dn 14
 number 213
!
ephone-dn 15
 number 214
!
ephone-dn 16
 number 215
!
ephone-dn 17
 number 216
!
ephone-dn 18
 number 217
!
ephone-dn 19
 number 218
!
ephone-dn 20
 number 219
!
ephone-dn 21
 number 220
!
ephone-dn 22
 number 221
!
ephone-dn 23
 number 222
!
ephone-dn 24
 number 223
!
ephone-dn 25
 number 224
!
ephone-dn 26
 number 225
!
ephone-dn 27
 number 226
!
ephone-dn 28
 number 227
!
ephone-dn 29
 number 228
!
ephone-dn 30
 number 229
!
ephone 1
 device-security-mode none
 mac-address 0001.64DA.E53C
 type 7960
 button 1:1
!
ephone 2
 device-security-mode none
 mac-address 0090.21E8.D4D0
 type 7960
 button 1:2
!
line con 0
!
line aux 0
!
line vty 0 4
 login
!
!
!
end
