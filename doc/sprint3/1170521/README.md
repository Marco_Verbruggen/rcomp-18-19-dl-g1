﻿RCOMP 2018-2019 Project - Sprint 3 - Member 1170521 folder
===========================================
# Network Table #
|VLAN|Rede|Brodcast|1º Válido|Máscara|
|----|----|--------|---------|-------|
|DMZ|172.23.98.0|172.23.98.127|172.23.98.1|255.255.255.128|
|Backbone|172.23.96.0|172.23.96.63|172.29.96.1|255.255.255.192|
|Floor 0|172.23.98.128|172.23.98.191|172.23.98.129|255.255.255.192|
|Floor 1|172.23.98.192|172.23.98.255|172.23.98.193|255.255.255.192|
|WIFI|172.23.99.0|172.23.99.31|172.23.99.1|255.255.255.224|
|VoIP|172.23.99.32|172.23.99.63|172.23.99.33|255.255.255.224|

# Configurations #
## Router_BuildingA ##
Current configuration : 2575 bytes
!
version 12.4
no service timestamps log datetime msec
no service timestamps debug datetime msec
no service password-encryption
!
hostname Router_BuildingA
!
!
!
!
ip dhcp excluded-address 172.23.98.1
ip dhcp excluded-address 172.23.98.129
ip dhcp excluded-address 172.23.98.193
ip dhcp excluded-address 172.23.99.1
ip dhcp excluded-address 172.23.99.33
!
ip dhcp pool FLOOR0
 network 172.23.98.128 255.255.255.192
 default-router 172.23.98.129
 dns-server 8.8.8.8
 domain-name dei.isep.ipp.pt
ip dhcp pool FLOOR1
 network 172.23.98.192 255.255.255.192
 default-router 172.23.98.193
 dns-server 8.8.8.8
 domain-name dei.isep.ipp.pt
ip dhcp pool WIFI
 network 172.23.99.0 255.255.255.224
 default-router 172.23.99.1
 dns-server 8.8.8.8
 domain-name dei.isep.ipp.pt
ip dhcp pool VOICE
 network 172.23.99.32 255.255.255.224
 default-router 172.23.99.33
 option 150 ip 172.23.99.33
 dns-server 8.8.8.8
 domain-name dei.isep.ipp.pt
!
!
!
ip cef
no ipv6 cef
!
!
!
!
!
!
!
!
!
!
!
!
spanning-tree mode pvst
!
!
!
!
!
!
interface FastEthernet0/0
 no ip address
 duplex auto
 speed auto
!
interface FastEthernet0/0.491
 encapsulation dot1Q 491
 ip address 172.23.98.129 255.255.255.192
!
interface FastEthernet0/0.492
 encapsulation dot1Q 492
 ip address 172.23.98.193 255.255.255.192
!
interface FastEthernet0/0.493
 encapsulation dot1Q 493
 ip address 172.23.99.1 255.255.255.224
!
interface FastEthernet0/0.494
 encapsulation dot1Q 494
 ip address 172.23.98.1 255.255.255.128
!
interface FastEthernet0/0.495
 encapsulation dot1Q 495
 ip address 172.23.99.33 255.255.255.224
!
interface FastEthernet0/1
 ip address 172.23.96.2 255.255.255.192
 duplex auto
 speed auto
!
interface Vlan1
 no ip address
 shutdown
!
ip classless
ip route 0.0.0.0 0.0.0.0 172.23.96.1
!
ip flow-export version 9
!
!
!
!
!
!
!
!
telephony-service
 max-ephones 20
 max-dn 20
 ip source-address 172.23.99.33 port 2000
 auto assign 1 to 20
!
ephone-dn 1
 number 100
!
ephone-dn 2
 number 101
!
ephone-dn 3
 number 102
!
ephone-dn 4
 number 103
!
ephone-dn 5
 number 104
!
ephone-dn 6
 number 105
!
ephone-dn 7
 number 106
!
ephone-dn 8
 number 107
!
ephone-dn 9
 number 108
!
ephone-dn 10
 number 109
!
ephone-dn 11
 number 110
!
ephone-dn 12
 number 111
!
ephone-dn 13
 number 112
!
ephone-dn 14
 number 113
!
ephone-dn 15
 number 114
!
ephone-dn 16
 number 115
!
ephone-dn 17
 number 116
!
ephone-dn 18
 number 117
!
ephone-dn 19
 number 118
!
ephone 1
 device-security-mode none
 mac-address 0000.0C46.C391
 type 7960
 button 1:1
!
ephone 2
 device-security-mode none
 mac-address 0040.0BB0.D22E
 type 7960
 button 1:2
!
line con 0
!
line aux 0
!
line vty 0 4
 login
!
!
!
end


## Router_ISP ##
Current configuration : 667 bytes
!
version 12.4
no service timestamps log datetime msec
no service timestamps debug datetime msec
no service password-encryption
!
hostname Router_ISP
!
!
!
!
!
!
!
!
ip cef
no ipv6 cef
!
!
!
!
!
!
!
!
!
!
!
!
spanning-tree mode pvst
!
!
!
!
!
!
interface FastEthernet0/0
 ip address 190.5.200.94 255.255.255.252
 duplex auto
 speed auto
!
interface FastEthernet0/1
 no ip address
 duplex auto
 speed auto
 shutdown
!
interface Vlan1
 no ip address
 shutdown
!
ip classless
ip route 190.5.200.93 255.255.255.255 FastEthernet0/0
ip route 0.0.0.0 0.0.0.0 190.5.200.93
!
ip flow-export version 9
!
!
!
!
!
!
!
!
line con 0
!
line aux 0
!
line vty 0 4
 login
!
!
!
end
## Router_MC ##
Current configuration : 2363 bytes
!
version 12.4
no service timestamps log datetime msec
no service timestamps debug datetime msec
no service password-encryption
!
hostname Router_MC
!
!
!
!
!
!
!
!
no ip cef
no ipv6 cef
!
!
!
!
!
!
!
!
!
!
!
!
spanning-tree mode pvst
!
!
!
!
!
!
interface FastEthernet0/0
 ip address 172.23.96.1 255.255.255.192
 duplex auto
 speed auto
!
interface FastEthernet0/1
 ip address 190.5.200.93 255.255.255.252
 duplex auto
 speed auto
!
interface Modem0/3/0
 no ip address
!
interface Vlan1
 no ip address
 shutdown
!
ip classless
ip route 0.0.0.0 0.0.0.0 FastEthernet0/1
ip route 172.23.96.0 255.255.255.192 172.23.96.2
ip route 172.23.98.0 255.255.255.128 172.23.96.2
ip route 172.23.98.128 255.255.255.192 172.23.96.2
ip route 172.23.98.192 255.255.255.192 172.23.96.2
ip route 172.23.99.0 255.255.255.224 172.23.96.2
ip route 172.23.99.32 255.255.255.224 172.23.96.2
ip route 172.23.100.0 255.255.255.128 172.23.96.3
ip route 172.23.100.128 255.255.255.128 172.23.96.3
ip route 172.23.101.0 255.255.255.192 172.23.96.3
ip route 172.23.101.64 255.255.255.224 172.23.96.3
ip route 172.23.101.98 255.255.255.240 172.23.96.3
ip route 172.23.104.0 255.255.255.0 172.23.96.5
ip route 172.23.105.0 255.255.255.128 172.23.96.5
ip route 172.23.105.128 255.255.255.128 172.23.96.5
ip route 172.23.106.0 255.255.255.192 172.23.96.5
ip route 172.23.106.64 255.255.255.240 172.23.96.5
ip route 172.23.108.0 255.255.255.0 172.23.96.6
ip route 172.23.109.0 255.255.255.128 172.23.96.6
ip route 172.23.109.128 255.255.255.128 172.23.96.6
ip route 172.23.110.0 255.255.255.192 172.23.96.6
ip route 172.23.110.64 255.255.255.240 172.23.96.6
ip route 172.23.112.1 255.255.255.0 172.23.96.7
ip route 172.23.113.1 255.255.255.128 172.23.96.7
ip route 172.23.113.128 255.255.255.128 172.23.96.7
ip route 172.23.114.1 255.255.255.192 172.23.96.7
ip route 172.23.114.64 255.255.255.240 172.23.96.7
ip route 172.23.116.1 255.255.255.128 172.23.96.8
ip route 172.23.116.128 255.255.255.128 172.23.96.8
ip route 172.23.117.1 255.255.255.128 172.23.96.8
ip route 172.23.117.128 255.255.255.192 172.23.96.8
ip route 172.23.117.192 255.255.255.240 172.23.96.8
!
ip flow-export version 9
!
!
!
!
!
!
!
!
dial-peer voice 1 voip
 destination-pattern 1..
 session target ipv4:172.23.96.2
!
dial-peer voice 2 voip
 destination-pattern 2..
 session target ipv4:172.23.96.3
!
dial-peer voice 3 voip
 destination-pattern 3..
 session target ipv4:172.23.96.4
!
dial-peer voice 4 voip
 destination-pattern 4..
 session target ipv4:172.23.96.5
!
dial-peer voice 5 voip
 destination-pattern 5..
 session target ipv4:172.23.96.6
!
dial-peer voice 6 voip
 destination-pattern 6..
 session target ipv4:172.23.96.7
!
dial-peer voice 7 voip
 destination-pattern 7..
 session target ipv4:172.23.96.8
!
line con 0
!
line aux 0
!
line vty 0 4
 login
!
!
!
end
