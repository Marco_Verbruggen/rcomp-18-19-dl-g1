RCOMP 2018-2019 Project - Sprint 3 planning
===========================================
### Sprint master: 1170944 ###
# 1. Sprint's backlog #
In this sprint, each team member will keep working on the same network simulation from the previous sprint,  regarding  the  same  building.  The  focus  will  now  be  layer  three,  namely IPv4 routersand  layer three IPv4 networks, but will also encompass some network services like DHCP and VoIP.
# 2. Technical decisions and coordination #
* The default route should be with this ip address : 190.5.200.190 / 30;
* The model of the routers used should be the 2811;
* Every building should have one router;
* The default route should be the gateway to the internet;
* The device naming should follow this rules : DeviceType_Building;
* The VLAN naming should follow this rules : Building_Subnet (the subnets are referenced on the next table).


## Subnet table ##  

|Building|IP Range|Subnet|
|--------|--------|------|
|Backbone|172.23.128.0/26||
|A|172.23.98.0/25|DMZ|
|A|172.23.98.128/26|Floor 0|
|A|172.23.98.192/26|Floor 1|
|A|172.23.99.0/27|WIFI|
|A|172.23.99.32/27|VoIP|

|B|172.23.100.0/25|WIFI|
|B|172.23.100.128/25|Floor 1|
|B|172.23.101.0/26|Floor 0|
|B|172.23.101.64/27|VoIP|
|B|172.23.101.98/28|DMZ|

|C|172.23.99.128/25|WIFI|
|C|172.23.100.0/25|Floor 1|
|C|172.23.100.128/25|Floor 0|
|C|172.23.101.0/26|VoIP|
|C|172.23.101.64/28|DMZ|

|D|172.23.104.0/24|WIFI|
|D|172.23.105.0/25|VoIP|
|D|172.23.105.128/25|Floor 1|
|D|172.23.106.0/26|Floor 0|
|D|172.23.106.64/28|DMZ|

|E|172.23.108.0/24|WIFI|
|E|172.23.109.0/25|Floor 1|
|E|172.23.109.128/25|VoIP|
|E|172.23.110.0/26|Floor 0|
|E|172.23.110.64/28|DMZ|

|F|172.23.112.1/24|WIFI|
|F|172.23.113.1/25|Floor 1|
|F|172.23.113.128/25|VoIP|
|F|172.23.114.1/26|Floor 0|
|F|172.23.114.64/28|DMZ|

|G|172.23.116.1/25|Floor 1|
|G|172.23.116.128/25|WIFI|
|G|172.23.117.1/25|VoIP|
|G|172.23.117.128/26|Floor 0|
|G|172.23.117.192/28|DMZ|

## Phone Number Schema##
|Building|Prefix|Range|
|--------|------|-----|
|A|1|100-119|
|B|2|200-229|
|C|3|300-347|
|D|4|400-479|
|E|5|500-569|
|F|6|600-666|
|G|7|700-769|

# 3. Subtasks assignment #
|Task|Assignee|Task Description|
|----|--------|----------------|
|T.3.1|1170521|Development ofa layer threePacket Tracer simulation for building A, including DHCP and VoIP service, encompassing the campus backbone, and alsoan Internet connection.|
|T.3.2|1161274|Development of a layer three Packet Tracer simulation for building B, including  DHCP  and  VoIP  service,  and  also  encompassing  the  campus backbone.Integration of every members� Packet Tracer simulations into a single simulation.|
|T.3.3|Segmentation Fault|Development of a layer three Packet Tracer simulation for building , including  DHCP  and  VoIP  service,  and  also  encompassing  the  campus backbone.|
|T.3.4|1170944|Development of a layer three Packet Tracer simulation for building D, including  DHCP  and  VoIP  service,  and  also  encompassing  the  campus backbone.|
|T.3.5|1170677|Development of a layer three Packet Tracer simulation for building E, including  DHCP  and  VoIP  service,  and  also  encompassing  the  campus backbone.|
|T.3.6|1170614|Development of a layer three Packet Tracer simulation for building F, including  DHCP  and  VoIP  service,  and  also  encompassing  the  campus backbone.|
|T.3.7|1170623|Development of a layer three Packet Tracer simulation for building G, including  DHCP  and  VoIP  service,  and  also  encompassing  the  campus backbone.|
