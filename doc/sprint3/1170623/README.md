RCOMP 2018-2019 Project - Sprint 3 - Member 1170623 folder
===========================================

### Router_G0_1 config ###

!
version 12.4
no service timestamps log datetime msec
no service timestamps debug datetime msec
no service password-encryption
!
hostname Router_G0_1
!
!
!
!
ip dhcp excluded-address 172.29.110.129
ip dhcp excluded-address 172.29.111.1
ip dhcp excluded-address 172.29.111.129
ip dhcp excluded-address 172.29.112.1
ip dhcp excluded-address 172.29.112.65
!
ip dhcp pool FLOOR1
 network 172.29.110.128 255.255.255.128
 default-router 172.29.110.129
 dns-server 8.8.8.8
 domain-name dei.isep.ipp.pt
ip dhcp pool WIFI
 network 172.29.111.0 255.255.255.128
 default-router 172.29.111.1
 dns-server 8.8.8.8
 domain-name dei.isep.ipp.pt
ip dhcp pool VOIP
 network 172.29.111.128 255.255.255.128
 default-router 172.29.111.129
 option 150 ip 172.29.111.129
 dns-server 8.8.8.8
 domain-name dei.isep.ipp.pt
ip dhcp pool FLOOR0
 network 172.29.112.0 255.255.255.192
 default-router 172.29.112.1
 dns-server 8.8.8.8
 domain-name dei.isep.ipp.pt
!
!
!
no ip cef
no ipv6 cef
!
!
!
!
!
!
!
!
!
!
!
!
spanning-tree mode pvst
!
!
!
!
!
!
interface FastEthernet0/0
 ip address 172.29.96.7 255.255.255.192
 duplex auto
 speed auto
!
interface FastEthernet0/0.521
 encapsulation dot1Q 521
 ip address 172.29.112.1 255.255.255.192
!
interface FastEthernet0/0.522
 encapsulation dot1Q 522
 ip address 172.29.110.129 255.255.255.128
!
interface FastEthernet0/0.523
 encapsulation dot1Q 523
 ip address 172.29.111.1 255.255.255.128
!
interface FastEthernet0/0.524
 encapsulation dot1Q 524
 ip address 172.29.112.65 255.255.255.240
!
interface FastEthernet0/0.525
 encapsulation dot1Q 525
 ip address 172.29.111.129 255.255.255.128
!
interface FastEthernet0/1
 ip address 172.29.96.136 255.255.255.192
 duplex auto
 speed auto
!
interface FastEthernet1/0
 no ip address
 duplex auto
 speed auto
 shutdown
!
interface FastEthernet1/1
 no ip address
 duplex auto
 speed auto
 shutdown
!
interface Vlan1
 no ip address
 shutdown
!
ip classless
ip route 0.0.0.0 0.0.0.0 172.29.96.129 
!
ip flow-export version 9
!
!
!
!
!
!
!
!
dial-peer voice 1 voip
 destination-pattern 1..
 session target ipv4:172.29.96.1
!
dial-peer voice 2 voip
 destination-pattern 2..
 session target ipv4:172.29.96.2
!
dial-peer voice 3 voip
 destination-pattern 3..
 session target ipv4:172.29.96.3
!
dial-peer voice 4 voip
 destination-pattern 4..
 session target ipv4:172.29.96.4
!
dial-peer voice 5 voip
 destination-pattern 5..
 session target ipv4:172.29.96.5
!
dial-peer voice 6 voip
 destination-pattern 6..
 session target ipv4:172.29.96.6
!
telephony-service
 max-ephones 42
 max-dn 70
 ip source-address 172.29.111.129 port 2000
 auto assign 1 to 70
!
ephone-dn 1
 number 700
!
ephone-dn 2
 number 701
!
ephone-dn 3
 number 702
!
ephone-dn 4
 number 703
!
ephone-dn 5
 number 704
!
ephone-dn 6
 number 705
!
ephone-dn 7
 number 706
!
ephone-dn 8
 number 707
!
ephone-dn 9
 number 708
!
ephone-dn 10
 number 709
!
ephone-dn 11
 number 710
!
ephone-dn 12
 number 711
!
ephone-dn 13
 number 712
!
ephone-dn 14
 number 713
!
ephone-dn 15
 number 714
!
ephone-dn 16
 number 715
!
ephone-dn 17
 number 716
!
ephone-dn 18
 number 717
!
ephone-dn 19
 number 718
!
ephone-dn 20
 number 719
!
ephone-dn 21
 number 720
!
ephone-dn 22
 number 721
!
ephone-dn 23
 number 722
!
ephone-dn 24
 number 723
!
ephone-dn 25
 number 724
!
ephone-dn 26
 number 725
!
ephone-dn 27
 number 726
!
ephone-dn 28
 number 727
!
ephone-dn 29
 number 728
!
ephone-dn 30
 number 729
!
ephone-dn 31
 number 730
!
ephone-dn 32
 number 731
!
ephone-dn 34
 number 733
!
ephone-dn 33
 number 732
!
ephone-dn 35
 number 734
!
ephone-dn 36
 number 735
!
ephone-dn 37
 number 736
!
ephone-dn 38
 number 737
!
ephone-dn 39
 number 738
!
ephone-dn 40
 number 739
!
ephone-dn 41
 number 740
!
ephone-dn 42
 number 741
!
ephone-dn 43
 number 742
!
ephone-dn 44
 number 743
!
ephone-dn 45
 number 744
!
ephone-dn 46
 number 745
!
ephone-dn 47
 number 746
!
ephone-dn 48
 number 747
!
ephone-dn 49
 number 748
!
ephone-dn 50
 number 749
!
ephone-dn 51
 number 750
!
ephone-dn 52
 number 751
!
ephone-dn 53
 number 752
!
ephone-dn 54
 number 753
!
ephone-dn 55
 number 754
!
ephone-dn 57
 number 756
!
ephone-dn 56
 number 755
!
ephone-dn 58
 number 757
!
ephone-dn 59
 number 758
!
ephone-dn 60
 number 759
!
ephone-dn 61
 number 760
!
ephone-dn 62
 number 761
!
ephone-dn 63
 number 762
!
ephone-dn 64
 number 763
!
ephone-dn 65
 number 764
!
ephone-dn 66
 number 765
!
ephone-dn 67
 number 766
!
ephone-dn 68
 number 767
!
ephone-dn 69
 number 768
!
ephone-dn 70
 number 769
!
ephone 1
 device-security-mode none
 mac-address 0002.4ACE.0C8A
 type 7960
 button 1:1
!
ephone 2
 device-security-mode none
 mac-address 0060.5C3A.B7E4
 type 7960
 button 1:2
!
ephone 3
 device-security-mode none
 mac-address 0001.64C8.596C
 type 7960
 button 1:3
!
ephone 4
 device-security-mode none
 mac-address 00D0.BC85.61AA
 type 7960
 button 1:4
!
line con 0
!
line aux 0
!
line vty 0 4
 login
!
!
!
end


### Building G Network Table ###

|VLAN|Rede|Broadcast|1� V�lido|M�scara|
|----|----|---------|---------|-------|
|G_Floor0|172.29.112.0|172.29.112.63|172.29.112.1|/26 255.255.255.192|
|G_Floor1|172.29.110.128|172.29.110.255|172.29.110.129|/25 255.255.255.128|
|G_Wifi|172.29.111.0|172.29.111.127|172.29.111.1|/25 255.255.255.128|
|G_DMZ|172.29.112.64|172.29.112.79|172.29.112.65|/28 255.255.255.240|
|G_VoiP|172.29.111.128|172.29.111.255|172.29.111.129|/25 255.255.255.128|	