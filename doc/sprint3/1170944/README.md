Tabela de rede:  

|   VLAN  |      Rede      |      1º IP     |    Broadcast   | Mascara |
|:-------:|:--------------:|:--------------:|:--------------:|:-------:|
| Floor 0 |  172.29.104.0  |  172.29.104.1  | 172.229.104.63 |   /26   |
| Floor 1 | 172.29.103.128 | 172.29.103.129 | 172.29.103.255 |   /25   |
|   WIFI  |  172.29.102.0  |  172.29.102.1  | 172.29.102.255 |   /24   |
| DMZ     | 172.29.104.64  | 172.29.104.65  | 172.29.104.95  | /28     |
| VOIP    | 172.29.103.0   | 172.29.103.1   | 172.29.103.127 | /25     |




CONFIGS :

!
version 12.4
no service timestamps log datetime msec
no service timestamps debug datetime msec
no service password-encryption
!
hostname D_ROUTER
!
!
!
!
ip dhcp excluded-address 172.29.104.1
ip dhcp excluded-address 172.29.103.29
ip dhcp excluded-address 172.29.102.1
ip dhcp excluded-address 172.29.103.1
!
ip dhcp pool D0
 network 172.29.104.0 255.255.255.192
 default-router 172.29.104.1
 dns-server 8.8.8.8
 domain-name dei.isep.ipp.pt
ip dhcp pool D1
 network 172.29.103.128 255.255.255.128
 default-router 172.29.103.129
 dns-server 8.8.8.8
 domain-name dei.isep.ipp.pt
ip dhcp pool WIFI
 network 172.29.102.0 255.255.255.0
 default-router 172.29.102.1
 dns-server 8.8.8.8
 domain-name dei.isep.ipp.pt
ip dhcp pool VOIP
 network 172.29.103.0 255.255.255.128
 default-router 172.29.103.1
 option 150 ip 172.29.103.1
 dns-server 8.8.8.8
 domain-name dei.isep.ipp.pt
!
!
!
no ip cef
no ipv6 cef
!
!
!
!
!
!
!
!
!
!
!
!
spanning-tree mode pvst
!
!
!
!
!
!
interface FastEthernet0/0
 no ip address
 duplex auto
 speed auto
!
interface FastEthernet0/0.506
 encapsulation dot1Q 506
 ip address 172.29.104.1 255.255.255.192
!
interface FastEthernet0/0.507
 encapsulation dot1Q 507
 ip address 172.29.103.129 255.255.255.128
!
interface FastEthernet0/0.508
 encapsulation dot1Q 508
 ip address 172.29.102.1 255.255.255.0
!
interface FastEthernet0/0.509
 encapsulation dot1Q 509
 ip address 172.29.104.65 255.255.255.240
!
interface FastEthernet0/0.510
 encapsulation dot1Q 510
 ip address 172.29.103.1 255.255.255.128
!
interface FastEthernet0/1
 ip address 172.29.96.133 255.255.255.192
 duplex auto
 speed auto
!
interface Vlan1
 no ip address
 shutdown
!
ip classless
ip route 0.0.0.0 0.0.0.0 FastEthernet0/1
!
ip flow-export version 9
!
!
!
!
!
!
!
!
dial-peer voice 1 voip
 destination-pattern 1..
 session target ipv4:172.29.96.130
!
dial-peer voice 2 voip
 destination-pattern 2..
 session target ipv4:172.29.96.131
!
dial-peer voice 3 voip
 destination-pattern 3..
 session target ipv4:172.29.96.132
!
dial-peer voice 4 voip
 destination-pattern 4..
 session target ipv4:172.29.96.133
!
dial-peer voice 5 voip
 destination-pattern 5..
 session target ipv4:172.29.96.134
!
dial-peer voice 6 voip
 destination-pattern 6..
 session target ipv4:172.29.96.135
!
dial-peer voice 7 voip
 destination-pattern 7..
 session target ipv4:172.29.96.136
!
telephony-service
 max-ephones 42
 max-dn 100
 ip source-address 172.29.103.1 port 2000
 auto assign 40 to 100
 auto assign 1 to 100
!
ephone-dn 1
 number 401
!
ephone-dn 2
 number 402
!
ephone-dn 3
 number 403
!
ephone-dn 4
 number 404
!
ephone-dn 5
 number 405
!
ephone-dn 6
 number 406
!
ephone-dn 7
 number 407
!
ephone 1
 device-security-mode none
 mac-address 0030.F219.8168
 type 7960
 button 1:1
!
ephone 2
 device-security-mode none
 mac-address 0060.4784.B729
 type 7960
 button 1:2
!
line con 0
!
line aux 0
!
line vty 0 4
 login
!
!
!
end
