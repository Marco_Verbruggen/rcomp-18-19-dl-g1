RCOMP 2018-2019 Project - Sprint 3 - Member 11701614 folder
===========================================


# Configuration #
## Router BuildingF ##
!
version 12.4
no service timestamps log datetime msec
no service timestamps debug datetime msec
no service password-encryption
!
hostname Router_BuildingF
!
!
!
!
ip dhcp excluded-address 172.23.114.1
ip dhcp excluded-address 172.23.113.1
ip dhcp excluded-address 172.23.112.1
ip dhcp excluded-address 172.23.113.129
!
ip dhcp pool FLOOR0
 network 172.23.114.0 255.255.255.192
 default-router 172.23.114.1
 dns-server 8.8.8.8
 domain-name dei.isep.ipp.pt
ip dhcp pool FLOOR1
 network 172.23.113.0 255.255.255.128
 default-router 172.23.113.1
 dns-server 8.8.8.8
 domain-name dei.isep.ipp.pt
ip dhcp pool WIFI
 network 172.23.112.0 255.255.255.0
 default-router 172.23.112.1
 dns-server 8.8.8.8
 domain-name dei.isep.ipp.pt
ip dhcp pool VOIP
 network 172.23.113.128 255.255.255.128
 default-router 172.23.113.129
 dns-server 8.8.8.8
 domain-name dei.isep.ipp.pt
!
!
!
no ip cef
no ipv6 cef
!
!
!
!
!
!
!
!
!
!
!
!
spanning-tree mode pvst
!
!
!
!
!
!
interface FastEthernet0/0
 ip address 172.23.96.6 255.255.255.192
 duplex auto
 speed auto
!
interface FastEthernet0/1
 no ip address
 duplex auto
 speed auto
!
interface FastEthernet0/1.490
 encapsulation dot1Q 490
 no ip address
!
interface FastEthernet1/0
 no ip address
 duplex auto
 speed auto
 shutdown
!
interface FastEthernet1/1
 no ip address
 duplex auto
 speed auto
!
interface FastEthernet1/1.490
 encapsulation dot1Q 490
 no ip address
!
interface FastEthernet1/1.516
 encapsulation dot1Q 516
 ip address 172.23.114.1 255.255.255.192
!
interface FastEthernet1/1.517
 encapsulation dot1Q 517
 ip address 172.23.113.1 255.255.255.128
!
interface FastEthernet1/1.518
 encapsulation dot1Q 518
 ip address 172.23.112.1 255.255.255.0
!
interface FastEthernet1/1.519
 encapsulation dot1Q 519
 ip address 172.23.114.64 255.255.255.240
!
interface FastEthernet1/1.520
 encapsulation dot1Q 520
 ip address 172.23.113.129 255.255.255.128
!
interface Vlan1
 no ip address
 shutdown
!
ip classless
ip route 0.0.0.0 0.0.0.0 172.23.96.129
!
ip flow-export version 9
!
!
!
!
!
!
!
!
dial-peer voice 1 voip
 destination-pattern 1..
 session target ipv4:172.23.96.1
!
dial-peer voice 2 voip
 destination-pattern 2..
 session target ipv4:172.23.96.2
!
dial-peer voice 3 voip
 destination-pattern 3..
 session target ipv4:172.23.96.3
!
dial-peer voice 4 voip
 destination-pattern 4..
 session target ipv4:172.23.96.4
!
dial-peer voice 5 voip
 destination-pattern 5..
 session target ipv4:172.23.96.5
!
dial-peer voice 7 voip
 destination-pattern 7..
 session target ipv4:172.23.96.7
!
telephony-service
 max-ephones 42
 max-dn 67
 ip source-address 172.23.113.129 port 2000
 auto assign 1 to 67
!
ephone-dn 1
 number 600
!
ephone-dn 2
 number 601
!
ephone-dn 25
 number 624
!
ephone-dn 26
 number 625
!
ephone-dn 27
 number 626
!
ephone-dn 28
 number 627
!
ephone-dn 29
 number 628
!
ephone-dn 30
 number 629
!
ephone-dn 31
 number 630
!
ephone-dn 32
 number 631
!
ephone-dn 33
 number 632
!
ephone-dn 34
 number 633
!
ephone-dn 35
 number 634
!
ephone-dn 36
 number 635
!
ephone-dn 37
 number 636
!
ephone-dn 38
 number 637
!
ephone-dn 39
 number 638
!
ephone-dn 40
 number 639
!
ephone-dn 41
 number 640
!
ephone-dn 42
 number 641
!
ephone-dn 3
 number 602
!
ephone-dn 4
 number 603
!
ephone-dn 5
 number 604
!
ephone-dn 6
 number 605
!
ephone-dn 7
 number 606
!
ephone-dn 8
 number 607
!
ephone-dn 9
 number 608
!
ephone-dn 10
 number 609
!
ephone-dn 11
 number 610
!
ephone-dn 12
 number 611
!
ephone-dn 13
 number 612
!
ephone-dn 14
 number 613
!
ephone-dn 15
 number 614
!
ephone-dn 16
 number 615
!
ephone-dn 17
 number 616
!
ephone-dn 18
 number 617
!
ephone-dn 19
 number 618
!
ephone-dn 20
 number 619
!
ephone-dn 21
 number 620
!
ephone-dn 22
 number 621
!
ephone-dn 23
 number 622
!
ephone-dn 24
 number 623
!
ephone-dn 43
 number 642
!
ephone-dn 44
 number 643
!
ephone-dn 45
 number 644
!
ephone-dn 46
 number 645
!
ephone-dn 47
 number 646
!
ephone-dn 48
 number 647
!
ephone-dn 49
 number 649
!
ephone-dn 50
 number 650
!
ephone-dn 51
 number 651
!
ephone-dn 52
 number 652
!
ephone-dn 53
 number 653
!
ephone-dn 54
 number 654
!
ephone-dn 55
 number 655
!
ephone-dn 56
 number 656
!
ephone-dn 57
 number 657
!
ephone-dn 58
 number 658
!
ephone-dn 59
 number 659
!
ephone-dn 60
 number 660
!
ephone-dn 61
 number 661
!
ephone-dn 62
 number 663
!
ephone-dn 63
 number 664
!
ephone-dn 64
 number 665
!
ephone-dn 65
 number 666
!
ephone 1
 device-security-mode none
 mac-address 00D0.BA9D.D63A
 type 7960
 button 1:1
!
ephone 2
 device-security-mode none
 mac-address 0060.70B2.9A78
 type 7960
 button 1:2
!
line con 0
!
line aux 0
!
line vty 0 4
 login
!
!
!
end


## Router MC ##

!
version 12.4
no service timestamps log datetime msec
no service timestamps debug datetime msec
no service password-encryption
!
hostname Router_A0_1
!
!
!
enable secret 5 $1$mERr$3HhIgMGBA/9qNmgzccuxv0
enable password 1234
!
!
!
!
!
!
no ip cef
no ipv6 cef
!
!
!
!
!
!
!
!
!
!
!
!
spanning-tree mode pvst
!
!
!
!
!
!
interface FastEthernet0/0
 ip address 172.29.96.129 255.255.255.192
 duplex auto
 speed auto
!
interface FastEthernet0/0.490
 encapsulation dot1Q 490
 ip address 172.29.96.1 255.255.255.192
!
interface FastEthernet0/1
 no ip address
 duplex auto
 speed auto
 shutdown
!
interface FastEthernet1/0
 no ip address
!
interface Vlan1
 no ip address
 shutdown
!
ip classless
!
ip flow-export version 9
!
!
!
!
!
!
!
!
line con 0
!
line aux 0
!
line vty 0 4
 password 12345
 login
!
!
!
end

## Tabela ##
| VLAN | Rede | Broadcast | 1º válido | máscara |
|--------|-------|------|-------|--------|
|F - FLOOR0 | 172.29.110.0 | 172.29.110.63 | 172.29.110.1 | 255.255.255.192 |
|F - FLOOR1| 172.29.109.0 | 172.29.109.127 | 172.29.109.1 | 255.255.255.128 |
|F - WIFI| 172.29.108.0 | 172.29.108.255 | 172.29.108.1 | 255.255.255.0 |
|F - DMZ| 172.29.110.64 | 172.29.110.127 | 172.29.110.65 | 255.255.255.240 |
|F - VOIP| 172.29.109.128 | 172.29.109.255 | 172.29.109.129 | 255.255.255.128 |
