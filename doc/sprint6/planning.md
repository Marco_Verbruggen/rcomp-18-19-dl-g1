RCOMP 2018-2019 Project - Sprint 6 planning
===========================================
### Sprint master: 1170623 ###

# 1. Sprint's backlog #
  In  this final  sprint, each network  application resulting from  the  previous  sprint,  is  going  the  have  anHTTP server implementation added to it. The main objective is providing a satisfactoryuser interfacefor  interaction  with  the  application,  often  referred  to  as web-based  user  interfaces  or  web  user interfaces (WUI). For each application, thistask is assigned to the same team memberwho has been developing it in the previous sprint, so basically, members keep working in the development of the same application.Nevertheless, the first priorityfor each member, is having the network application operating as expected forthe end  of  sprint  5.  That  is  to  say,  the application  operates  as  part  of  a  system,  together  with  the network applications developed by the other team members.

# 2. Technical decisions and coordination #
  * Code Language for Frontend: JavaScript;
  * Code Language for Backend: Java;
  * Usage of MVC design pattern (Frontend: view and controller; Backend: model)
  * TODO: Choose base HTTP server from publicly available templates and base frontend JavaScript frameworks;

# 3. Subtasks assignment #
|Task|Assignee|Task Description|
|----|--------|----------------|
|T.3.1|1170521|Continue  the  development  of  the Hashapplication,  and  implement  the web-based user interface as described for this sprint’s requisites.|
|T.3.2|1161274|Continue the development of the UserAuthMD5application, and implement the web-based user interface as described for this sprint’s requisites.|
|T.3.3|Segmentation Fault|Continue the development of the Varapplication, and implement the web-based user interface as describedfor this sprint’s requisites.|
|T.3.4|1170944|Continue the development of the NumConvertapplication, and implement the web-based user interface as described for this sprint’s requisites|
|T.3.5|1170677|Continue the development of the Stackapplication, and implement the web-based user interface as described for this sprint’s requisites.|
|T.3.6|1170614|Continue the development of the TCPmonitorapplication, and implement the web-based user interface as described for this sprint’s requisites.|
|T.3.7|1170623|Continue the development of the Queueapplication, and implement the web-based user interface as described for this sprint’s requisites.|

