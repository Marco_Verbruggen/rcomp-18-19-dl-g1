RCOMP 2018-2019 Project - Sprint 6 review
=========================================
### Sprint master: 1170623 ###

## 1. Sprint's backlog ##

In  this final  sprint, each network  application resulting from  the  previous  sprint,  is  going  the  have  an HTTP server implementation added to it. The main objective is providing a satisfactoryuser interfacefor  interaction  with  the  application,  often  referred  to  as web-based  user  interfaces  or  web  user interfaces (WUI). For each application, thistask is assigned to the same team memberwho has been developing it in the previous sprint, so basically, members keep working in the development of the same application.Nevertheless, the first priorityfor each member, is having the network application operating as expected forthe end  of  sprint  5.  That  is  to  say,  the application  operates  as  part  of  a  system,  together  with  the network applications developed by the other team members.

## 2. Subtasks assessment ##

#2.1 1170521 - Continue  the  development  of  the Hashapplication,  and  implement  the web-based user interface as described for this sprint’s requisites. #

- __Not Implemented__
- Did not know how to implement HTTP server and JavaScript.
- Could not fix problems with previous iteration.

# 2.2. 11161274 - Continue the development of the UserAuthMD5application, and implement the web-based user interface as described for this sprint’s requisites. #

- __Not Implemented__
- Did not know how to implement HTTP server and JavaScript.
- Could not fix problems with previous iteration.

# 2.3. Segmentation Fault - Continue the development of the Varapplication, and implement the web-based user interface as describedfor this sprint’s requisites. #

- __Not Implemented__
- Did not know how to implement HTTP server and JavaScript.
- Could not fix problems with previous iteration.

# 2.4. 1170944 - Continue the development of the NumConvertapplication, and implement the web-based user interface as described for this sprint’s requisites #

- __Not Implemented__
- Did not know how to implement HTTP server and JavaScript.
- Could not fix problems with previous iteration.

# 2.5. 1170677 - Continue the development of the Stackapplication, and implement the web-based user interface as described for this sprint’s requisites. #

- __Not Implemented__
- Did not know how to implement HTTP server and JavaScript.
- Could not fix problems with previous iteration.

# 2.6. 1170614 - Continue the development of the TCPmonitorapplication, and implement the web-based user interface as described for this sprint’s requisites. #

- __Not Implemented__
- Did not know how to implement HTTP server and JavaScript.
- Could not fix problems with previous iteration.

# 2.7. 1170623 - Continue the development of the Queueapplication, and implement the web-based user interface as described for this sprint’s requisites. #

- __Not Implemented__
- Did not know how to implement HTTP server and JavaScript.
- Could not fix problems with previous iteration.
