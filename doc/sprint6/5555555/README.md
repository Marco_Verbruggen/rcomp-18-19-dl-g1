RCOMP 2018-2019 Project - Sprint 6 - Member 5555555 folder
===========================================
(This folder is to be created/edited by the team member 5555555 only)

#### This is just an example for a team member with number 5555555 ####
### Each member should create a folder similar to this, matching his/her number. ###
The owner of this folder (member number 5555555) will commit here all the outcomes (results/artifacts/products)		       of his/her work during sprint 6. This may encompass any kind of standard file types.
