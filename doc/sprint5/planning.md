RCOMP 2018-2019 Project - Sprint 5 planning
===========================================
### Sprint master: 1170614 ###

# 1. Sprint's backlog #
In this sprint,each team member will work on one of seven different applications.

# 2. Technical decisions and coordination #
## Division of applications ##
__Var__ - 1170521

__UserAuthMD5__ - 1161274

__NumConvert__ - 1170944

__Stack__ - 1170677

__TCPmonitor__ - 1170614

__Queue__  - 1170623

## UDP Port ##

- __31201__

# 3. Subtasks assignment #
|Task|Assignee|Task description|
|-|-|
|T.5.1|1170521|Take part on the design of the core protocol specification and develop a shared implementation for it (co-development). Establish the usermessages format for the Var application (3.1.), and implement such an application.|
|T.5.2|1161274|Take part on the design of the core protocol specification and develop a shared implementation for it (co-development). Establish the usermessages format for the UserAuthMD5 application (3.2.), and implement such an application.|
|T.5.3|NullPoiterException|Take part on the design of the core protocol specification and develop a shared implementation for it (co-development). Establish the usermessages format for the Hash application (3.3.), and implement such an application.|
|T.5.4|1170944|Take part on the design of the core protocol specification and develop a shared implementation for it (co-development). Establish the usermessages format for the NumConvert application (3.4.), and implement such an application.|
|T.5.5|1170677|Take part on the design of the core protocol specification and develop a shared implementation for it (co-development). Establish the usermessages format for the Stack application (3.5), and implement such an application.|
|T.5.6|1170614|Take part on the design of the core protocol specification and develop a shared implementation for it (co-development). Establish the usermessages format for the TCPmonitor application (3.6), and implement such an application.|
|T.5.7|1170623|Take part on the design of the core protocol specification and develop a shared implementation for it (co-development). Establish the usermessages format for the Queue application (3.7), and implement such an application.|
