RCOMP 2018-2019 Project - Sprint 5 - Member 6666666 folder
===========================================
(This folder is to be created/edited by the team member 6666666 only)

#### This is just an example for a team member with number 6666666 ####
### Each member should create a folder similar to this, matching his/her number. ###
The owner of this folder (member number 6666666) will commit here all the outcomes (results/artifacts/products)		       of his/her work during sprint 5. This may encompass any kind of standard file types.
