RCOMP 2018-2019 Project - Sprint 5 review
=========================================
### Sprint master: 1170614 ###

# 1. Sprint's backlog #
In this sprint,each team member will work on one of seven different applications.
# 2. Subtasks assessment #
One by one, each team member presents his/her outcomes to the team, the team assesses the accomplishment of the subtask backlog.
The subtask backlog accomplishment can be assessed as one of:

  * Totally implemented with no issues
  * Totally implemented with issues
  * Partially implemented with no issues
  * Partially implemented with issues

For the last three cases, a text description of what has not been implemented and present issues must be added.
Unimplemented features and issues solving is assigned to the same member on the next sprint.

## 2.1. 1170521 - Take part on the design of the core protocol specification and develop a shared implementation for it (co-development). Establish the usermessages format for the Hash application (3.3.), and implement such an application. #
- __Partially implemented with issues.__
Implemented the application without problems. The common part was implemented with issues.

## 2.2. 1161274 - Take part on the design of the core protocol specification and develop a shared implementation for it (co-development). Establish the usermessages format for the UserAuthMD5 application (3.2.), and implement such an application. #
- __Partially implemented with issues.__
Implemented the application without problems. The common part was implemented with issues.

## 2.3. 1170944 - Take part on the design of the core protocol specification and develop a shared implementation for it (co-development). Establish the usermessages format for the NumConvert application (3.4.), and implement such an application. #
- __Partially implemented with issues.__
Implemented the application without problems. The common part was implemented with issues.

## 2.4. 1170677 - Take part on the design of the core protocol specification and develop a shared implementation for it (co-development). Establish the usermessages format for the Stack application (3.5), and implement such an application. #
- __Partially implemented with issues.__
Implemented the application without problems. The common part was implemented with issues.

## 2.5. 1170614 - Take part on the design of the core protocol specification and develop a shared implementation for it (co-development). Establish the usermessages format for the TCPmonitor application (3.6), and implement such an application. #
- __Partially implemented with issues.__
Implemented the application without problems. The common part was implemented with issues.

## 2.6. 1170623 - Take part on the design of the core protocol specification and develop a shared implementation for it (co-development). Establish the usermessages format for the Queue application (3.7), and implement such an application.
- __Partially implemented with issues.__
Implemented the application without problems. The common part was implemented with issues.
