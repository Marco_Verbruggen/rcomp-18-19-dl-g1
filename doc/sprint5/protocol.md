# PROTOCOL  
## Requisits
* The Protocol that out team is going to use is UDP, port number **31201**;
* All messages exchanged between applications are plain text CR (carriage return) terminated lines, and each is transported by a single UDP datagram;
* Maximum total size of a message is 500 bytes (CRs included);
* Every application manages a list of active applications within the system. This list contains application names, and for each, the corresponding IP address.

  # Messages
    * Applications are supposed to ignore an incoming message if the source port number is not correct.
    * The interpretation of the message’s content is up to the application that receives it, if not understood a comprehensive error message should be sent in response.


* **System Messages** must be understood by every application;
* **User Messages** are understood by one application type only;
* System-messages can’t get mixed together with user-messages (sent following user’s interaction).

## System Messages
System messages are intended for the system to operate and not for final users, thus when a user interacts through input commands, system-message must not interfere and will not arise.  
System messages can’t get mixed together with user-messages (sent following user’s interaction).
System messages have the mechanism to alert all applications of each other.
The format of the system message when an application starts running: Name of the application - IP IPAddress
Example: IP address /172.18.154.193 in the application HashApplication joined 


## User Messages
Can only be specified to an application type and can only be understood by that application type.
