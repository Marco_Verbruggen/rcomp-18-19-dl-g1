﻿RCOMP 2018-2019 Project - Sprint 2 planning
===========================================
### Sprint master: 1170521 ###
# 1. Sprint's backlog #
All team members are required to create a network simulation matching the created structured cabling project. In this sprint the focus is on layer two, e.g. switches and access-points, but some end devices are going to be added in preparation for the next sprint.

# 2. Technical decisions and coordination #
* Every cross connect(including CP) must be represented with switches
* Cable types must match;
* Redundancy should be included(except link aggregation);
* VLAN'scorrectly addressed and assemble in any switch;
* DON'T disable STP;
* Assign VTP properly to all devices(domain,server,client);
* Each VLAN must have at least one device (service + VOIP + wireless laptop associated with acess point, workstation per floor).
* VTP domain name: vtdlg1;
* Convention to hostname/display name: Intermediate Cross should be named "IC Building [X]", Horizontal Cross Gonnects should be named "HC [X].0" and "HC [X].1" for the floor 0 and 1. Consolidation Points should be named "CP [X].[floor].[n room]";
* Convention to devices display name: [Equipment Name][Building][.Floor]

### List of Vlans: ###

| ID | Nome |
|----|------|
|490	| CampusBackbone |
|491	|APiso0|
|492	|APiso1|
|493	|AWifi|
|494	|ADMZ|
|495	|AVoIP|
|496	|BPiso0|
|497	|BPiso1|
|498	|BWifi|
|499	|BDMZ|
|500	|BVoIP|
|501	|CPiso0|
|502	|CPiso1|
|503	|CWifi|
|504	|CDMZ|
|505	|CVoIP|
|506	|DPiso0|
|507	|DPiso1|
|508	|DWifi|
|509	|DDMZ|
|510	|DVoIP|
|511	|EPiso0|
|512	|EPiso1|
|513	|EWifi|
|514	|EDMZ|
|515	|EVoIP|
|516	|FPiso0|
|517	|FPiso1|
|518	|FWifi|
|519	|FDMZ|
|520	|FVoIP|
|521	|GPiso0|
|522	|GPiso1|
|523	|GWifi|
|524	|GDMZ|
|525	|GVoIP|


# 3. Subtasks assignment #

| Task  | Assigne | Task Description                                                                                                                                                                                    |
|-------|---------|-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| T.2.1 | 1170521 | Development of a layer two Packet Tracer  simulation for building A, and also encompassing the campus backbone.  Integration of every member's Packet Tracer simulations  into a single simulation. |
| T.2.2 | 1161274 |Development of a layer two Packet Tracer simulation for building B, and also encompassing the campus backbone.                                                                                      |
| T.2.3 | 1160723 |Development of a layer two Packet Tracer simulation for building C, and also encompassing the campus backbone.                                                                                      |
| T.2.4 | 1170944 |Development of a layer two Packet Tracer simulation for building D, and also encompassing the campus backbone                                                                                       |
| T.2.5 | 1170677 |Development of a layer two Packet Tracer simulation for building E, and also encompassing the campus backbone.                                                                                      |
| T.2.6 | 1170614 |Development of a layer two Packet Tracer simulation for building F, and also encompassing the campus backbone                                                                                       |
| T.2.7 | 1170623 |Development of a layer two Packet Tracer simulation for building G, and also encompassing the campus backbone.                                                                                      |
