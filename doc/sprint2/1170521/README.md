RCOMP 2018-2019 Project - Sprint 2 - Member 170521 folder
===========================================

# CONFIGURATIONS #
## BuildingA ##
### MCBuildingA ###
Current configuration : 775 bytes
!
version 12.1
no service timestamps log datetime msec
no service timestamps debug datetime msec
no service password-encryption
!
hostname MCBuildingA
!
!
!
!
!
spanning-tree mode pvst
spanning-tree extend system-id
!
interface FastEthernet0/1
 switchport mode trunk
!
interface FastEthernet1/1
 switchport mode trunk
!
interface FastEthernet2/1
 switchport mode trunk
!
interface FastEthernet3/1
 switchport mode trunk
!
interface FastEthernet4/1
 switchport mode trunk
!
interface FastEthernet5/1
 switchport mode trunk
!
interface FastEthernet6/1
 switchport mode trunk
!
interface FastEthernet7/1
!
interface FastEthernet8/1
!
interface FastEthernet9/1
!
interface Vlan1
 no ip address
 shutdown
!
!
!
!
line con 0
!
line vty 0 4
 login
line vty 5 15
 login
!
!
!
!
end

### ICBuildingA ###
Current configuration : 729 bytes
!
version 12.1
no service timestamps log datetime msec
no service timestamps debug datetime msec
no service password-encryption
!
hostname ICBuildingA
!
!
!
!
!
spanning-tree mode pvst
spanning-tree extend system-id
!
interface FastEthernet0/1
 switchport mode trunk
!
interface FastEthernet1/1
 switchport mode trunk
!
interface FastEthernet2/1
 switchport mode trunk
!
interface FastEthernet3/1
 switchport mode trunk
!
interface FastEthernet4/1
 switchport mode trunk
!
interface FastEthernet5/1
!
interface FastEthernet6/1
!
interface FastEthernet7/1
!
interface FastEthernet8/1
!
interface FastEthernet9/1
!
interface Vlan1
 no ip address
 shutdown
!
!
!
!
line con 0
!
line vty 0 4
 login
line vty 5 15
 login
!
!
!
!
end

### HCA.0 ###
Current configuration : 862 bytes
!
version 12.1
no service timestamps log datetime msec
no service timestamps debug datetime msec
no service password-encryption
!
hostname HCA.0
!
!
!
!
!
spanning-tree mode pvst
spanning-tree extend system-id
!
interface FastEthernet0/1
 switchport mode trunk
!
interface FastEthernet1/1
 switchport mode trunk
!
interface FastEthernet2/1
!
interface FastEthernet3/1
!
interface FastEthernet4/1
 switchport access vlan 493
 switchport mode access
!
interface FastEthernet5/1
 switchport access vlan 493
 switchport mode access
!
interface FastEthernet6/1
 switchport access vlan 491
 switchport mode access
!
interface FastEthernet7/1
 switchport access vlan 494
 switchport mode access
!
interface FastEthernet8/1
!
interface FastEthernet9/1
!
interface Vlan1
 no ip address
 shutdown
!
!
!
!
line con 0
!
line vty 0 4
 login
line vty 5 15
 login
!
!
!
!
end

### HCA.1 ###
Current configuration : 631 bytes
!
version 12.1
no service timestamps log datetime msec
no service timestamps debug datetime msec
no service password-encryption
!
hostname HCA.1
!
!
!
!
!
spanning-tree mode pvst
spanning-tree extend system-id
!
interface FastEthernet0/1
!
interface FastEthernet1/1
 switchport mode trunk
!
interface FastEthernet2/1
!
interface FastEthernet3/1
!
interface FastEthernet4/1
!
interface FastEthernet5/1
!
interface FastEthernet6/1
!
interface FastEthernet7/1
!
interface FastEthernet8/1
!
interface FastEthernet9/1
!
interface Vlan1
 no ip address
 shutdown
!
!
!
!
line con 0
!
line vty 0 4
 login
line vty 5 15
 login
!
!
!
!
end


### CPA.1.2 ###
Current configuration : 788 bytes
!
version 12.1
no service timestamps log datetime msec
no service timestamps debug datetime msec
no service password-encryption
!
hostname CPA.1.2
!
!
!
!
!
spanning-tree mode pvst
spanning-tree extend system-id
!
interface FastEthernet0/1
 switchport mode trunk
!
interface FastEthernet1/1
!
interface FastEthernet2/1
!
interface FastEthernet3/1
 switchport access vlan 493
 switchport mode access
!
interface FastEthernet4/1
 switchport access vlan 492
 switchport mode access
!
interface FastEthernet5/1
 switchport mode access
 switchport voice vlan 495
!
interface FastEthernet6/1
!
interface FastEthernet7/1
!
interface FastEthernet8/1
!
interface FastEthernet9/1
!
interface Vlan1
 no ip address
 shutdown
!
!
!
!
line con 0
!
line vty 0 4
 login
line vty 5 15
 login
!
!
!
!
end

## Backbone ##
### ICBuildingB ###
Current configuration : 515 bytes
!
version 12.1
no service timestamps log datetime msec
no service timestamps debug datetime msec
no service password-encryption
!
hostname ICBuildingB
!
!
!
!
!
spanning-tree mode pvst
spanning-tree extend system-id
!
interface FastEthernet0/1
 switchport mode trunk
!
interface FastEthernet1/1
 switchport mode trunk
!
interface FastEthernet2/1
 switchport mode trunk
!
interface FastEthernet3/1
!
interface Vlan1
 no ip address
 shutdown
!
!
!
!
line con 0
!
line vty 0 4
 login
line vty 5 15
 login
!
!
!
!
end

### ICBuildingC ###
Current configuration : 515 bytes
!
version 12.1
no service timestamps log datetime msec
no service timestamps debug datetime msec
no service password-encryption
!
hostname ICBuildingC
!
!
!
!
!
spanning-tree mode pvst
spanning-tree extend system-id
!
interface FastEthernet0/1
 switchport mode trunk
!
interface FastEthernet1/1
 switchport mode trunk
!
interface FastEthernet2/1
 switchport mode trunk
!
interface FastEthernet3/1
!
interface Vlan1
 no ip address
 shutdown
!
!
!
!
line con 0
!
line vty 0 4
 login
line vty 5 15
 login
!
!
!
!
end

### ICBuildingD ###
Current configuration : 515 bytes
!
version 12.1
no service timestamps log datetime msec
no service timestamps debug datetime msec
no service password-encryption
!
hostname ICBuildingD
!
!
!
!
!
spanning-tree mode pvst
spanning-tree extend system-id
!
interface FastEthernet0/1
 switchport mode trunk
!
interface FastEthernet1/1
 switchport mode trunk
!
interface FastEthernet2/1
 switchport mode trunk
!
interface FastEthernet3/1
!
interface Vlan1
 no ip address
 shutdown
!
!
!
!
line con 0
!
line vty 0 4
 login
line vty 5 15
 login
!
!
!
!
end

### ICBuildingE ###
Current configuration : 515 bytes
!
version 12.1
no service timestamps log datetime msec
no service timestamps debug datetime msec
no service password-encryption
!
hostname ICBuildingE
!
!
!
!
!
spanning-tree mode pvst
spanning-tree extend system-id
!
interface FastEthernet0/1
 switchport mode trunk
!
interface FastEthernet1/1
 switchport mode trunk
!
interface FastEthernet2/1
 switchport mode trunk
!
interface FastEthernet3/1
!
interface Vlan1
 no ip address
 shutdown
!
!
!
!
line con 0
!
line vty 0 4
 login
line vty 5 15
 login
!
!
!
!
end


### ICBuildingF ###
Current configuration : 515 bytes
!
version 12.1
no service timestamps log datetime msec
no service timestamps debug datetime msec
no service password-encryption
!
hostname ICBuildingF
!
!
!
!
!
spanning-tree mode pvst
spanning-tree extend system-id
!
interface FastEthernet0/1
 switchport mode trunk
!
interface FastEthernet1/1
 switchport mode trunk
!
interface FastEthernet2/1
 switchport mode trunk
!
interface FastEthernet3/1
!
interface Vlan1
 no ip address
 shutdown
!
!
!
!
line con 0
!
line vty 0 4
 login
line vty 5 15
 login
!
!
!
!
end

### ICBuildingG ###
Current configuration : 515 bytes
!
version 12.1
no service timestamps log datetime msec
no service timestamps debug datetime msec
no service password-encryption
!
hostname ICBuildingG
!
!
!
!
!
spanning-tree mode pvst
spanning-tree extend system-id
!
interface FastEthernet0/1
 switchport mode trunk
!
interface FastEthernet1/1
 switchport mode trunk
!
interface FastEthernet2/1
 switchport mode trunk
!
interface FastEthernet3/1
!
interface Vlan1
 no ip address
 shutdown
!
!
!
!
line con 0
!
line vty 0 4
 login
line vty 5 15
 login
!
!
!
!
end