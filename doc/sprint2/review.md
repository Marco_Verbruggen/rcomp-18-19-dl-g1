RCOMP 2018-2019 Project - Sprint 2 review
=========================================
### Sprint master: 1170521 ###
# 1. Sprint's backlog #
All team members are required to create a network simulation matching the created structured cabling project.
In this sprint the focus is on layer two, e.g. switches and access-points, but some end devices are going to be added in preparation for the next sprint.
# 2. Subtasks assessment #
One by one, each team member presents his/her outcomes to the team, the team assesses the accomplishment of the subtask backlog.
The subtask backlog accomplishment can be assessed as one of:

  * Totally implemented with no issues
  * Totally implemented with issues
  * Partially implemented with no issues
  * Partially implemented with issues

For the last three cases, a text description of what has not been implemented and present issues must be added.
Unimplemented features and issues solving is assigned to the same member on the next sprint.

## 2.1. 1170521 -  Development of a layer two Packet Tracer simulation for building A, and also encompassing the campus backbone. Integration of every member's Packet Tracer simulations into a single simulation. #
### Totally implemented with no issues. ###
## 2.2. 1161274 - Development of a layer two Packet Tracer simulation for building B, and also encompassing the campus backbone. #
### Totally implemented with issues. ###
## 2.4. 1170944 - Development of a layer two Packet Tracer simulation for building D, and also encompassing the campus backbone. #
### Totally implemented with issues. ###
## 2.5. 1170677 - Development of a layer two Packet Tracer simulation for building E, and also encompassing the campus backbone. #
### Totally implemented with issues. ###
## 2.6. 1170614 - Development of a layer two Packet Tracer simulation for building F, and also encompassing the campus backbone. #
### Totally implemented with issues. ###
## 2.7. 1170623 - Development of a layer two Packet Tracer simulation for building G, and also encompassing the campus backbone. #
### Totally implemented with issues. ###

## Justifications ##
Although it was not possible to test the connections on the simulation because every end node was in a different VLAN, we temporary changed two ports to the same VLAN and sent a package between the two end nodes and it was succesfull.
