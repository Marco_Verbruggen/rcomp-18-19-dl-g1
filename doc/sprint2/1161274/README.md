RCOMP 2018-2019 Project - Sprint 2 - Member 2222222 folder
===========================================


##IC B0.2
hostname IC_B0.2
vtp mode client
vtp domain vtdlg1
interface fastEthernet0/1
switchport mode trunk
interface fastEthernet1/1
switchport mode trunk

##HC B0.2
hostname HC_B0.2
vtp mode client
vtp domain vtdlg1
interface fastEthernet0/1
switchport mode trunk
interface fastEthernet1/1
switchport mode trunk
interface Ethernet2/1
switchport mode access
switchport access vlan 499

##HC B1.2
hostname HC_B1.2
vtp mode client
vtp domain vtdlg1
interface fastEthernet0/1
switchport mode trunk
interface fastEthernet1/1
switchport mode trunk
interface fastEthernet2/1
switchport mode trunk
interface fastEthernet3/1
switchport mode trunk

##CP B0.3
hostname CP_B0.3
vtp mode client
vtp domain vtdlg1
interface fastEthernet0/1
switchport mode trunk
interface Ethernet1/1
switchport mode access
switchport access vlan 496

##CP B1.1
hostname CP_B1.1
vtp mode client
vtp domain vtdlg1
interface fastEthernet0/1
switchport mode trunk
interface fastEthernet1/1
switchport mode trunk
interface fastEthernet2/1
switchport mode trunk
interface Ethernet3/1
switchport mode access
switchport access vlan 497

##CP B1.5
hostname CP_B1.5
vtp mode client
vtp domain vtdlg1
interface fastEthernet0/1
switchport mode trunk
interface fastEthernet1/1
switchport mode trunk
interface fastEthernet2/1
switchport mode trunk
interface Ethernet3/1
switchport mode access
switchport voice vlan 500


##CP B1.8
hostname CP_B1.8
vtp mode client
vtp domain vtdlg1
interface fastEthernet0/1
switchport mode trunk
interface fastEthernet1/1
switchport mode trunk
interface fastEthernet2/1
switchport mode trunk
interface fastEthernet3/1
switchport mode access
switchport access vlan 498
