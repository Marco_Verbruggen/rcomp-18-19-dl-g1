RCOMP 2018-2019 Project - Sprint 2 - Member 1170944 folder
===========================================

#MC - MESMA QUE O DO BACKBONE

##IC
hostname ic
vtp mode client
vtp domain vtdlg1
interface fastEthernet0/1
switchport mode trunk
interface fastEthernet1/1
switchport mode trunk

##HC0
hostname HC0
vtp mode client
vtp domain vtdlg1
interface fastEthernet0/1
switchport mode trunk
interface fastEthernet1/1
switchport mode trunk
interface fastEthernet2/1
switchport mode trunk
interface fastEthernet3/1
switchport mode trunk
interface fastEthernet4/1
switchport mode trunk
interface fastEthernet5/1
switchport mode trunk
interface fastEthernet6/1
switchport mode trunk
interface fastEthernet7/1
switchport mode access
switchport access vlan 508
interface fastEthernet8/1
switchport mode access
switchport access vlan 508
##HC1_1
hostname HC1_1
vtp mode client
vtp domain vtdlg1
interface fastEthernet0/1
switchport mode trunk
interface fastEthernet1/1
switchport mode trunk
interface fastEthernet2/1
switchport mode trunk
interface fastEthernet3/1
switchport mode trunk
interface fastEthernet4/1
switchport mode trunk
interface fastEthernet5/1
switchport mode access
switchport access vlan 508
interface fastEthernet6/1
switchport mode access
switchport access vlan 508
##HC1_2
hostname HC1_2
vtp mode client
vtp domain vtdlg1
interface fastEthernet0/1
switchport mode trunk
interface fastEthernet1/1
switchport mode trunk
interface fastEthernet2/1
switchport mode trunk
interface fastEthernet3/1
switchport mode trunk
interface fastEthernet4/1
switchport mode trunk
interface fastEthernet5/1
switchport mode access
switchport access vlan 508
interface fastEthernet6/1
switchport mode access
switchport access vlan 508
##HC1_3
hostname HC1_3
vtp mode client
vtp domain vtdlg1
interface fastEthernet0/1
switchport mode trunk
interface fastEthernet1/1
switchport mode trunk
interface fastEthernet2/1
switchport mode trunk
interface fastEthernet3/1
switchport mode trunk
interface fastEthernet4/1
switchport mode trunk
interface fastEthernet5/1
switchport mode access
switchport access vlan 508
interface fastEthernet6/1
switchport mode access
switchport access vlan 508
##CP_0_1
hostname CP0_1
vtp mode client
vtp domain vtdlg1
interface fastEthernet0/1
switchport mode trunk
interface fastEthernet1/1
switchport mode trunk
interface fastEthernet2/1
switchport mode trunk
interface fastEthernet3/1
switchport mode access
switchport access vlan 506

##CP_0_2
hostname CP0_2
vtp mode client
vtp domain vtdlg1
interface fastEthernet0/1
switchport mode trunk
interface fastEthernet1/1
switchport mode trunk
interface fastEthernet2/1
switchport mode trunk
interface fastEthernet3/1
switchport mode access
switchport access vlan 509

##CP_0_3
hostname CP0_3
vtp mode client
vtp domain vtdlg1
interface fastEthernet0/1
switchport mode trunk
interface fastEthernet1/1
switchport mode trunk
interface fastEthernet2/1
switchport mode trunk
interface fastEthernet3/1
no switchport mode access
switchport voice vlan 510

##CP_1_1
hostname CP1_1
vtp mode client
vtp domain vtdlg1
interface fastEthernet0/1
switchport mode trunk
interface fastEthernet1/1
switchport mode trunk
interface fastEthernet2/1
switchport mode trunk
interface fastEthernet3/1
switchport mode access
switchport access vlan 507

##CP_1_2
hostname CP1_2
vtp mode client
vtp domain vtdlg1
interface fastEthernet0/1
switchport mode trunk
interface fastEthernet1/1
switchport mode trunk
interface fastEthernet2/1
switchport mode trunk
interface fastEthernet3/1
switchport mode access
switchport access vlan 509

##CP_1_3
hostname CP1_3
vtp mode client
vtp domain vtdlg1
interface fastEthernet0/1
switchport mode trunk
interface fastEthernet1/1
switchport mode trunk
interface fastEthernet2/1
switchport mode trunk
interface fastEthernet3/1
no switchport mode access
switchport voice vlan 510