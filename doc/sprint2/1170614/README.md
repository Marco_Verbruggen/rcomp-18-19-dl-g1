RCOMP 2018-2019 Project - Sprint 2 - Member 1170614 folder
===========================================

### ICbuildingF0.8
!
version 12.1
no service timestamps log datetime msec
no service timestamps debug datetime msec
no service password-encryption
!
hostname ICbuildingF0.8
!
!
!
!
!
spanning-tree mode pvst
spanning-tree extend system-id
!
interface FastEthernet4/1
 switchport mode trunk
!
interface FastEthernet5/1
 switchport mode trunk
!
interface FastEthernet6/1
 switchport mode trunk
!
interface FastEthernet7/1
!
interface FastEthernet8/1
 switchport mode trunk
!
interface FastEthernet9/1
 switchport mode trunk
!
interface Vlan1
 no ip address
 shutdown
!
!
!
!
line con 0
!
line vty 0 4
 login
line vty 5 15
 login
!
!
!
!
end

### HCbuildingF0.8
!
version 12.1
no service timestamps log datetime msec
no service timestamps debug datetime msec
no service password-encryption
!
hostname HCbuildingF0.8
!
!
!
!
!
spanning-tree mode pvst
spanning-tree extend system-id
!
interface Ethernet0/1
 switchport access vlan 519
 switchport mode access
!
interface Ethernet1/1
 switchport access vlan 519
!
interface Ethernet2/1
!
interface Ethernet3/1
!
interface Ethernet4/1
!
interface Ethernet5/1
!
interface Ethernet6/1
!
interface FastEthernet7/1
 switchport mode trunk
!
interface FastEthernet8/1
!
interface FastEthernet9/1
 switchport mode trunk
!
interface Vlan1
 no ip address
 shutdown
!
!
!
!
line con 0
!
line vty 0 4
 login
line vty 5 15
 login
!
!
!
!
end


### CPbuildingF0.5
!
version 12.1
no service timestamps log datetime msec
no service timestamps debug datetime msec
no service password-encryption
!
hostname CPbuildingF0.5
!
!
!
!
!
spanning-tree mode pvst
spanning-tree extend system-id
!
interface Ethernet0/1
!
interface Ethernet1/1
!
interface Ethernet2/1
!
interface Ethernet3/1
!
interface Ethernet4/1
 switchport access vlan 516
 switchport mode access
!
interface Ethernet5/1
!
interface Ethernet6/1
!
interface FastEthernet7/1
 switchport mode trunk
!
interface FastEthernet8/1
 switchport mode trunk
!
interface FastEthernet9/1
 switchport mode trunk
!
interface Vlan1
 no ip address
 shutdown
!
!
!
!
line con 0
!
line vty 0 4
 login
line vty 5 15
 login
!
!
!
!
end

### HCbuildingF1.3

!
version 12.1
no service timestamps log datetime msec
no service timestamps debug datetime msec
no service password-encryption
!
hostname HCbuildingF1.3
!
!
!
!
!
spanning-tree mode pvst
spanning-tree extend system-id
!
interface Ethernet0/1
!
interface Ethernet1/1
!
interface Ethernet2/1
!
interface Ethernet3/1
!
interface Ethernet4/1
!
interface Ethernet5/1
!
interface Ethernet6/1
 switchport access vlan 517
 switchport mode access
!
interface FastEthernet7/1
!
interface FastEthernet8/1
!
interface FastEthernet9/1
!
interface Vlan1
 no ip address
 shutdown
!
!
!
!
line con 0
!
line vty 0 4
 login
line vty 5 15
 login
!
!
!
!
end

### HCbuildingF1.17
!
version 12.1
no service timestamps log datetime msec
no service timestamps debug datetime msec
no service password-encryption
!
hostname HCbuildingF1.17
!
!
!
!
!
spanning-tree mode pvst
spanning-tree extend system-id
!
interface Ethernet0/1
!
interface Ethernet1/1
!
interface Ethernet2/1
!
interface Ethernet3/1
!
interface Ethernet4/1
!
interface Ethernet5/1
!
interface FastEthernet6/1
 switchport access vlan 518
 switchport mode access
!
interface FastEthernet7/1
!
interface FastEthernet8/1
!
interface FastEthernet9/1
 switchport mode trunk
!
interface Vlan1
 no ip address
 shutdown
!
!
!
!
line con 0
!
line vty 0 4
 login
line vty 5 15
 login
!
!
!
!
end


### HCbuildingF1.22
!
version 12.1
no service timestamps log datetime msec
no service timestamps debug datetime msec
no service password-encryption
!
hostname HCbuildingF1.17
!
!
!
!
!
spanning-tree mode pvst
spanning-tree extend system-id
!
interface Ethernet0/1
 switchport mode access
 switchport voice vlan 520
!
interface Ethernet1/1
!
interface Ethernet2/1
!
interface Ethernet3/1
!
interface Ethernet4/1
!
interface Ethernet5/1
!
interface Ethernet6/1
!
interface FastEthernet7/1
!
interface FastEthernet8/1
 switchport mode trunk
!
interface FastEthernet9/1
 switchport mode trunk
!
interface Vlan1
 no ip address
 shutdown
!
!
!
!
line con 0
!
line vty 0 4
 login
line vty 5 15
 login
!
!
!
!
end
