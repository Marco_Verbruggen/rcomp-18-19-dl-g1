!
version 12.4
no service timestamps log datetime msec
no service timestamps debug datetime msec
no service password-encryption
!
hostname D_ROUTER
!
!
!
!
ip dhcp excluded-address 172.29.104.1
ip dhcp excluded-address 172.29.103.29
ip dhcp excluded-address 172.29.102.1
ip dhcp excluded-address 172.29.103.1
ip dhcp excluded-address 172.23.105.1
ip dhcp excluded-address 172.23.105.129
ip dhcp excluded-address 172.23.106.1
ip dhcp excluded-address 172.23.106.65
ip dhcp excluded-address 172.23.104.1
!
ip dhcp pool D0
 network 172.23.106.0 255.255.255.192
 default-router 172.23.106.1
 dns-server 172.23.106.66
 domain-name building-D.rcomp-18-19-dl-g1
ip dhcp pool D1
 network 172.23.105.128 255.255.255.128
 default-router 172.23.105.129
 dns-server 172.23.106.66
 domain-name building-D.rcomp-18-19-dl-g1
ip dhcp pool WIFI
 network 172.23.104.0 255.255.255.0
 default-router 172.23.104.1
 dns-server 172.23.106.66
 domain-name building-D.rcomp-18-19-dl-g1
ip dhcp pool VOIP
 network 172.23.105.0 255.255.255.128
 default-router 172.23.105.1
 option 150 ip 172.23.105.1
 dns-server 172.23.106.66
 domain-name building-D.rcomp-18-19-dl-g1
!
!
!
no ip cef
no ipv6 cef
!
!
!
!
!
!
!
!
!
!
!
!
spanning-tree mode pvst
!
!
!
!
!
!
interface FastEthernet0/0
 no ip address
 ip nat inside
 duplex auto
 speed auto
!
interface FastEthernet0/0.506
 encapsulation dot1Q 506
 ip address 172.23.106.1 255.255.255.192
!
interface FastEthernet0/0.507
 encapsulation dot1Q 507
 ip address 172.23.105.129 255.255.255.128
!
interface FastEthernet0/0.508
 encapsulation dot1Q 508
 ip address 172.23.104.1 255.255.255.0
!
interface FastEthernet0/0.509
 encapsulation dot1Q 509
 ip address 172.23.106.65 255.255.255.240
!
interface FastEthernet0/0.510
 encapsulation dot1Q 510
 ip address 172.23.105.1 255.255.255.128
!
interface FastEthernet0/1
 ip address 172.23.96.5 255.255.255.192
 ip nat outside
 duplex auto
 speed auto
!
interface Vlan1
 no ip address
 shutdown
!
router ospf 1
 log-adjacency-changes
 network 172.23.104.0 0.0.0.255 area 4
 network 172.23.105.0 0.0.0.127 area 4
 network 172.23.105.128 0.0.0.127 area 4
 network 172.23.106.0 0.0.0.63 area 4
 network 172.23.106.64 0.0.0.15 area 4
!
ip nat inside source list 1 interface FastEthernet0/1 overload
ip nat inside source static tcp 172.23.106.67 80 172.23.96.5 80 
ip nat inside source static tcp 172.23.106.67 443 172.23.96.5 443 
ip default-gateway 172.29.96.129
ip classless
ip route 0.0.0.0 0.0.0.0 FastEthernet0/1 
!
ip flow-export version 9
!
!
!
!
!
!
!
!
dial-peer voice 1 voip
 destination-pattern 1..
 session target ipv4:172.29.96.130
!
dial-peer voice 2 voip
 destination-pattern 2..
 session target ipv4:172.29.96.131
!
dial-peer voice 3 voip
 destination-pattern 3..
 session target ipv4:172.29.96.132
!
dial-peer voice 4 voip
 destination-pattern 4..
 session target ipv4:172.29.103.1
!
dial-peer voice 5 voip
 destination-pattern 5..
 session target ipv4:172.29.96.134
!
dial-peer voice 6 voip
 destination-pattern 6..
 session target ipv4:172.29.96.135
!
dial-peer voice 7 voip
 destination-pattern 7..
 session target ipv4:172.29.96.136
!
telephony-service
 max-ephones 42
 max-dn 100
 ip source-address 172.29.103.1 port 2000
 auto assign 40 to 100
 auto assign 1 to 100
!
ephone-dn 1
 number 401
!
ephone-dn 2
 number 402
!
ephone-dn 3
 number 403
!
ephone-dn 4
 number 404
!
ephone-dn 5
 number 405
!
ephone-dn 6
 number 406
!
ephone-dn 7
 number 407
!
ephone 1
 device-security-mode none
 mac-address 0030.F219.8168
 type 7960
 button 1:1
!
ephone 2
 device-security-mode none
 mac-address 0060.4784.B729
 type 7960
 button 1:2
!
line con 0
!
line aux 0
!
line vty 0 4
 login
!
!
!
end

