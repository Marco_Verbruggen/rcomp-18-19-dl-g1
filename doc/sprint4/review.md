RCOMP 2018-2019 Project - Sprint 4 review
=========================================
### Sprint master: 11707677###

# 1. Sprint's backlog #
In this sprint,each team member will keep working on the same network simulation from the previous sprint, regarding the same building. From the already established layer three configurations, now OSPF based dynamic routing will beused to replace static routingused before.
# 2. Subtasks assessment #
One by one, each team member presents his/her outcomes to the team, the team assesses 		the accomplishment of the subtask backlog.
The subtask backlog accomplishment can be assessed as one of:

  * Totally implemented with no issues
  * Totally implemented with issues
  * Partially implemented with no issues
  * Partially implemented with issues

For the last three cases, a text description of what has not been implemented and present issues must be added.
Unimplemented features and issues solving is assigned to the same member on the next sprint.

## 2.1. 1170521 - Update the campus.pkt layer three Packet  Tracer simulation  from  the previous sprint, to include the described features for this sprint for building A. #
- __Partially implemented with issues.__
- It was a little bit confusing implmenting DNS.
- There was a lack of information about NAT implementation.

## 2.2. 1161274 - Update the campus.pkt layer three Packet  Tracer simulation  from  the previous sprint, to include the described features for this sprint for building B. #
- __Partially implemented with issues.__

## 2.3. 1170944 - Update the campus.pkt layer three Packet  Tracer simulation  from  the previous sprint, to include the described features for this sprint for building D. Final integration of every members’ Packet Tracer simulations into a single simulation.#
- __Partially implemented with issues.__
- The assignment was not very clear.
- Contents like OSPF and ACL's cannot be explained only in 2 Laboratory Classes.

## 2.4. 1170677 - Update the campus.pkt layer three Packet  Tracer simulation  from  the previous sprint, to include the described features for this sprint for building E. #
- __Partially implemented with issues.__
- Difuculties in DNS implementation and ACL implementation. Additional difuculties in task interpretation.

## 2.5. 1170614 - Update the campus.pkt layer three Packet  Tracer simulation  from  the previous sprint, to include the described features for this sprint for building F. #
- __Partially implemented with issues.__
- Difuculties in DNS implementation and ACL implementation.

## 2.6. 1170623 - Update the campus.pkt layer three Packet  Tracer simulation  from  the previous sprint, to include the described features for this sprint for building G. #
- __Partially implemented with issues.__
- Could not determine the reason for the DNS service to not function, as all configurations and settings seemed to be correct. Additionally, could not implement other features required for the sprint due to the DNS server not functioning.
