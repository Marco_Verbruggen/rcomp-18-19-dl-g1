RCOMP 2018-2019 Project - Sprint 4 - Member 1161274 folder
===========================================
!
version 12.4
no service timestamps log datetime msec
no service timestamps debug datetime msec
no service password-encryption
!
hostname Router
!
!
!
!
ip dhcp excluded-address 172.23.100.1
ip dhcp excluded-address 172.23.100.129
ip dhcp excluded-address 172.23.101.1
ip dhcp excluded-address 172.23.101.120
!
ip dhcp pool WIFI
 network 172.23.100.0 255.255.255.128
 default-router 172.23.100.1
 dns-server 172.23.117.194
 domain-name bulding-b.rcomp-18-19-dl-g1
ip dhcp pool FLOOR1
 network 172.23.100.128 255.255.255.128
 default-router 172.23.100.129
 dns-server 172.23.117.194
 domain-name bulding-b.rcomp-18-19-dl-g1
ip dhcp pool FLOOR0
 network 172.23.101.0 255.255.255.128
 default-router 172.23.101.1
 dns-server 172.23.117.194
 domain-name bulding-b.rcomp-18-19-dl-g1
ip dhcp pool VOIP
 network 172.23.101.128 255.255.255.192
 default-router 172.23.101.129
 option 150 ip 172.23.101.129
 dns-server 172.23.117.194
 domain-name bulding-b.rcomp-18-19-dl-g1
!
!
!
no ip cef
no ipv6 cef
!
!
!
!
!
!
!
!
!
!
!
!
spanning-tree mode pvst
!
!
!
!
!
!
!
interface FastEthernet0/0.490
 encapsulation dot1Q 490
 ip address 172.23.96.3 255.255.255.192
!
interface FastEthernet0/1
 no ip address
 duplex auto
 speed auto
!
interface FastEthernet0/1.496
 encapsulation dot1Q 496
 ip address 172.23.101.1 255.255.255.128
!
interface FastEthernet0/1.497
 encapsulation dot1Q 497
 ip address 172.23.100.129 255.255.255.128
!
interface FastEthernet0/1.498
 encapsulation dot1Q 498
 ip address 172.23.100.1 255.255.255.128
!
interface FastEthernet0/1.499
 encapsulation dot1Q 499
 ip address 172.23.101.161 255.255.255.240
!
interface FastEthernet0/1.500
 encapsulation dot1Q 500
 ip address 172.23.101.129 255.255.255.192
!
interface FastEthernet1/0
 no ip address
 duplex auto
 speed auto
 shutdown
!
interface FastEthernet1/1
 no ip address
 duplex auto
 speed auto
 shutdown
!
interface Vlan1
 no ip address
 shutdown
!
router ospf 1
log-adjacency-changes
network 172.23.100.0 0.0.0.127 area 2
network 172.23.100.128 0.0.0.127 area 2
network 172.23.101.0 0.0.0.127 area 2
network 172.23.101.128 0.0.0.63 area 2
network 172.23.101.160 0.0.0.15 area 2
!
ip classless
ip route 0.0.0.0 0.0.0.0 172.23.96.1
!
ip flow-export version 9
!
!
!
!
!
!
!
!
dial-peer voice 1 voip
 destination-pattern 1..
 session target ipv4:172.23.96.1
!
dial-peer voice 2 voip
 destination-pattern 2..
 session target ipv4:172.23.96.2
!
dial-peer voice 3 voip
 destination-pattern 3..
 session target ipv4:172.23.96.3
!
dial-peer voice 4 voip
 destination-pattern 4..
 session target ipv4:172.23.96.4
!
dial-peer voice 5 voip
 destination-pattern 5..
 session target ipv4:172.23.96.5
!
dial-peer voice 7 voip
 destination-pattern 7..
 session target ipv4:172.23.96.7
!
telephony-service
 max-ephones 30
 max-dn 30
 ip source-address 172.23.101.129 port 2000
 auto assign 1 to 30
!
ephone-dn 1
 number 200
!
ephone-dn 2
 number 201
!
ephone-dn 3
 number 202
!
ephone-dn 4
 number 203
!
ephone-dn 5
 number 204
!
ephone-dn 6
 number 205
!
ephone-dn 7
 number 206
!
ephone-dn 8
 number 207
!
ephone-dn 9
 number 208
!
ephone-dn 10
 number 209
!
ephone-dn 11
 number 210
!
ephone-dn 12
 number 211
!
ephone-dn 13
 number 212
!
ephone-dn 14
 number 213
!
ephone-dn 15
 number 214
!
ephone-dn 16
 number 215
!
ephone-dn 17
 number 216
!
ephone-dn 18
 number 217
!
ephone-dn 19
 number 218
!
ephone-dn 20
 number 219
!
ephone-dn 21
 number 220
!
ephone-dn 22
 number 221
!
ephone-dn 23
 number 222
!
ephone-dn 24
 number 223
!
ephone-dn 25
 number 224
!
ephone-dn 26
 number 225
!
ephone-dn 27
 number 226
!
ephone-dn 28
 number 227
!
ephone-dn 29
 number 228
!
ephone-dn 30
 number 229
!
ephone 1
 device-security-mode none
 mac-address 0001.64DA.E53C
 type 7960
 button 1:1
!
ephone 2
 device-security-mode none
 mac-address 0090.21E8.D4D0
 type 7960
 button 1:2
!
line con 0
!
line aux 0
!
line vty 0 4
 login
!
!
!
end
