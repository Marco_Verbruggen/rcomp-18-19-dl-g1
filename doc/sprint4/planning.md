﻿RCOMP 2018-2019 Project - Sprint 4 planning
===========================================
### Sprint master: 1170677 ###

# 1. Sprint's backlog #
In this sprint,each team member will keep working on the same network simulation from the previous sprint, regarding the same building. From the already established layer three configurations, now OSPF based dynamic routing will beused to replace static routingused before.

__Other configuration tasks encompassed in this sprint will be:__
- Adding a second server to each DMZ network to run the HTTP service.
- Configuring DNS servers to establish a DNS domains tree.
- Enforcing NAT (Network Address Translation).
- Establishing traffic access policies (static firewall) on routers.

# 2. Technical decisions and coordination #

|Building|OSPF Area|DNS Server|DNS Domain Name|DNS Domain IP
|-|-|-|
|Backbone|0|||
|Building A|1|rcomp-18-19-dl-g1|ns.rcomp-18-19-dl-g1|172.23.98.2|
|Building B|2|building-b.rcomp-18-19-dl-g1|ns.building-b.rcomp-18-19-dl-g1|172.23.101.100|
|Building C|3|building-c.rcomp-18-19-dl-g1|ns.building-c.rcomp-18-19-dl-g1||
|Building D|4|building-d.rcomp-18-19-dl-g1|ns.building-d.rcomp-18-19-dl-g1|172.23.106.66|
|Building E|5|building-e.rcomp-18-19-dl-g1|ns.building-e.rcomp-18-19-dl-g1|172.23.110.66|
|Building F|6|building-f.rcomp-18-19-dl-g1|ns.building-f.rcomp-18-19-dl-g1|172.23.114.66|
|Building G|7|building-g.rcomp-18-19-dl-g1|ns.building-g.rcomp-18-19-dl-g1|172.23.117.194|

# 3. Subtasks assignment #

|Task|Assignee|Task description|
|-|-|
|T.4.1|1170521|Update the campus.pkt layer three Packet  Tracer simulation  from  the previous sprint, to include the described features for this sprint for building A.|
|T.4.2|1161274|Update the campus.pkt layer three Packet  Tracer simulation  from  the previous sprint, to include the described features for this sprint for building B.|
|T.4.3|NullPoiterException|Update the campus.pkt layer three Packet  Tracer simulation  from  the previous sprint, to include the described features for this sprint for building C.|
|T.4.4|1170944|Update the campus.pkt layer three Packet  Tracer simulation  from  the previous sprint, to include the described features for this sprint for building D. Final integration of every members’ Packet Tracer simulations into a single simulation.|
|T.4.5|1170677|Update the campus.pkt layer three Packet  Tracer simulation  from  the previous sprint, to include the described features for this sprint for building E.|
|T.4.6|1170614|Update the campus.pkt layer three Packet  Tracer simulation  from  the previous sprint, to include the described features for this sprint for building F.|
|T.4.7|1170623|Update the campus.pkt layer three Packet  Tracer simulation  from  the previous sprint, to include the described features for this sprint for building G.|
