RCOMP 2018-2019 Project - Sprint 4 - Member 1170677 folder
===========================================

!
version 12.4
no service timestamps log datetime msec
no service timestamps debug datetime msec
no service password-encryption
!
hostname Router_BuildingE
!
!
!
!
ip dhcp excluded-address 172.23.110.1
ip dhcp excluded-address 172.23.109.1
ip dhcp excluded-address 172.23.108.1
ip dhcp excluded-address 172.23.109.129
!
ip dhcp pool FLOOR1
 network 172.23.109.0 255.255.255.128
 default-router 172.23.109.1
 dns-server 172.23.110.67
 domain-name building-e.rcomp-18-19-dl-g1
ip dhcp pool VOIP
 network 172.23.109.128 255.255.255.128
 default-router 172.23.109.129
 option 150 ip 172.23.109.129
 dns-server 172.23.110.67
 domain-name building-e.rcomp-18-19-dl-g1
ip dhcp pool WIFI
 network 172.23.108.0 255.255.255.0
 default-router 172.23.108.1
 dns-server 172.23.110.67
 domain-name building-e.rcomp-18-19-dl-g1
ip dhcp pool FLOOR0
 network 172.23.110.0 255.255.255.192
 default-router 172.23.110.1
 dns-server 172.23.110.67
 domain-name building-e.rcomp-18-19-dl-g1
!
!
!
ip cef
no ipv6 cef
!
!
!
!
!
!
!
!
!
!
!
!
spanning-tree mode pvst
!
!
!
!
!
!
interface FastEthernet0/0
 ip address 172.23.96.6 255.255.255.192
 ip nat outside
 duplex auto
 speed auto
!
interface FastEthernet0/1
 no ip address
 ip nat inside
 duplex auto
 speed auto
!
interface FastEthernet0/1.511
 encapsulation dot1Q 511
 ip address 172.23.110.1 255.255.255.192
!
interface FastEthernet0/1.512
 encapsulation dot1Q 512
 ip address 172.23.109.1 255.255.255.128
!
interface FastEthernet0/1.513
 encapsulation dot1Q 513
 ip address 172.23.108.1 255.255.255.0
!
interface FastEthernet0/1.514
 encapsulation dot1Q 514
 ip address 172.23.110.65 255.255.255.240
!
interface FastEthernet0/1.515
 encapsulation dot1Q 515
 ip address 172.23.109.129 255.255.255.128
!
interface Vlan1
 no ip address
 shutdown
!
router ospf 1
 log-adjacency-changes
 network 172.23.110.64 0.0.0.15 area 5
 network 172.23.108.0 0.0.0.255 area 5
 network 172.23.109.128 0.0.0.127 area 5
 network 172.23.109.0 0.0.0.127 area 5
 network 172.23.110.0 0.0.0.63 area 5
!
ip nat inside source static tcp 172.23.110.66 80 172.23.96.6 80
ip nat inside source static tcp 172.23.110.66 443 172.23.96.6 443
ip nat inside source static tcp 172.23.110.67 53 172.23.96.6 53
ip nat inside source static udp 172.23.110.67 53 172.23.96.6 53
ip default-gateway 172.23.96.1
ip classless
ip route 0.0.0.0 0.0.0.0 172.23.96.1
!
ip flow-export version 9
!
!
!
!
!
!
!
!
dial-peer voice 1 voip
 destination-pattern 1..
 session target ipv4:172.23.96.2
!
dial-peer voice 2 voip
 destination-pattern 2..
 session target ipv4:172.23.96.3
!
dial-peer voice 3 voip
 destination-pattern 3..
 session target ipv4:172.23.96.4
!
dial-peer voice 4 voip
 destination-pattern 4..
 session target ipv4:172.23.96.5
!
dial-peer voice 5 voip
 destination-pattern 5..
 session target ipv4:172.23.109.129
!
dial-peer voice 6 voip
 destination-pattern 6..
 session target ipv4:172.23.96.7
!
dial-peer voice 7 voip
 destination-pattern 7..
 session target ipv4:172.23.96.8
!
telephony-service
 max-ephones 42
 max-dn 70
 auto assign 1 to 70
!
ephone-dn 1
 number 500
!
ephone-dn 2
 number 501
!
ephone-dn 3
 number 502
!
ephone-dn 4
 number 503
!
ephone-dn 5
 number 504
!
ephone-dn 6
 number 505
!
ephone-dn 7
 number 506
!
ephone-dn 8
 number 507
!
ephone-dn 9
 number 508
!
ephone-dn 10
 number 509
!
ephone-dn 11
 number 510
!
ephone-dn 12
 number 511
!
ephone-dn 13
 number 512
!
ephone-dn 14
 number 513
!
ephone-dn 15
 number 514
!
ephone-dn 16
 number 515
!
ephone-dn 17
 number 516
!
ephone-dn 18
 number 517
!
ephone-dn 19
 number 518
!
ephone-dn 20
 number 519
!
ephone-dn 21
 number 520
!
ephone-dn 22
 number 521
!
ephone-dn 23
 number 522
!
ephone-dn 24
 number 523
!
ephone-dn 25
 number 524
!
ephone-dn 26
 number 525
!
ephone-dn 27
 number 526
!
ephone-dn 28
 number 527
!
ephone-dn 29
 number 528
!
ephone-dn 30
 number 529
!
ephone-dn 31
 number 530
!
ephone-dn 32
 number 531
!
ephone-dn 33
 number 532
!
ephone-dn 34
 number 533
!
ephone-dn 35
 number 534
!
ephone-dn 36
 number 535
!
ephone-dn 37
 number 536
!
ephone-dn 38
 number 537
!
ephone-dn 39
 number 538
!
ephone-dn 40
 number 539
!
ephone-dn 41
 number 540
!
ephone-dn 42
 number 541
!
ephone-dn 43
 number 542
!
ephone-dn 44
 number 543
!
ephone-dn 45
 number 544
!
ephone-dn 46
 number 545
!
ephone-dn 47
 number 546
!
ephone-dn 48
 number 547
!
ephone-dn 49
 number 548
!
ephone-dn 50
 number 549
!
ephone-dn 51
 number 550
!
ephone-dn 52
 number 551
!
ephone-dn 53
 number 552
!
ephone-dn 54
 number 553
!
ephone-dn 55
 number 554
!
ephone-dn 56
 number 555
!
ephone-dn 57
 number 556
!
ephone-dn 58
 number 557
!
ephone-dn 59
 number 558
!
ephone-dn 60
 number 559
!
ephone-dn 61
 number 560
!
ephone-dn 62
 number 561
!
ephone-dn 63
 number 562
!
ephone-dn 64
 number 563
!
ephone-dn 65
 number 564
!
ephone-dn 66
 number 565
!
ephone-dn 67
 number 566
!
ephone-dn 68
 number 567
!
ephone-dn 69
 number 568
!
ephone-dn 70
 number 569
!
line con 0
!
line aux 0
!
line vty 0 4
 login
!
!
!
end
